﻿using UnityEngine;
using System.Collections;

namespace Codevember16
{
    public class Boundary : MonoBehaviour
    {
        public Vector3 BoundariesScales = new Vector3(18, 32, 24);

        // Use this for initialization
        void Awake()
        {
            this.gameObject.transform.localScale = BoundariesScales;
        }

        void OnTriggerExit()
        {
            //Debug.Log("exited");
        }
    }
}