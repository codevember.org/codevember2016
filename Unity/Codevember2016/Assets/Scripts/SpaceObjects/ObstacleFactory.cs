using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEngine;
using Random = System.Random;

namespace Codevember16
{
    [ExecuteInEditMode]
    public class ObstacleFactory : MonoBehaviourSingleton<ObstacleFactory>
    {
        public GameObject ObstaclePrefab;

        public GameObject NegativePolarizedObstacle;
        public GameObject PositivePolarizedObstacle;
        public GameObject NeutralPolarizedObstacle;

        public int ObstaclesCount = 2;

        public List<GameObject> Obstacles = new List<GameObject>();

        public Vector2 GravitySphereRadiusRange = new Vector2(1f, 2.5f);
        public Vector2 GravityAttractionStrengthRange = new Vector2(1f, 2.5f);
        public Vector2 MassRange = new Vector2(1f, 20f);

        public Vector2 ObstaclePositionRange = new Vector2(1536, 2048);

        protected override void Awake()
        {
            if (!Application.isPlaying)
            {
                if (!_instance)
                {
                    base.Awake();
                }
            }

            //Debug.Log(this.name + " has been awakened!");
        }

        public void GenerateRandomObstacles()
        {
            JsonConvert.DefaultSettings = (() =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter
                {
                    CamelCaseText = false
                });
                return settings;
            });

            Obstacles.Clear();
            var randy = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < ObstaclesCount; i++)
            {
                var polarity = (Polarity) randy.Next(0, 2);
                var go = Instantiate(GetPrefabForPolarity(polarity), this.transform) as GameObject;
                Obstacles.Add(go);
                var so = go.GetComponent<SpaceObject>();
                Debug.Log("GOP null? " + (so.Positioner == null));
                so.Positioner.SheepzWorldSpacePosition = new Vector2(
                    randy.Next(0, (int) ObstaclePositionRange.x),
                    randy.Next(0, (int) ObstaclePositionRange.y));

                // Assign random polarity.
                so.Polarity = polarity;
                so.IsAttractive = true;

                // Assign random values to the gravity
                var gravity = so.Gravity;
                gravity.Range = randy.NextFloat(GravitySphereRadiusRange.x, GravitySphereRadiusRange.y);
                gravity.StrengthOfAttraction = randy.NextFloat(GravityAttractionStrengthRange.x,
                    GravityAttractionStrengthRange.y);

                // Assign random mass to the gravity.
                gravity.GetComponent<Rigidbody>().mass = randy.NextFloat(MassRange.x, MassRange.y);


                var j = JsonConvert.SerializeObject(so, Formatting.Indented);
                Debug.Log(j);
            }
        }

        public SpaceObject CreateSheepzObjectFromJson(JsonSheepzsObject json, Transform parent = null)
        {
            //Debug.Log("Create from JSON " + json.Name);
            GameObject go;
            if (parent == null)
            {
                go = Instantiate(GetPrefabForPolarity(json.Polarity));
            }
            else
            {
                go = Instantiate(GetPrefabForPolarity(json.Polarity), parent) as GameObject;
            }

            var so = go.GetComponent<SpaceObject>();
            so.CopyValues(json);
            return so;
        }


        public SpaceObject CreateSheepzObjectFromJson(string json, Transform parent = null)
        {
            var obj = JsonConvert.DeserializeObject<JsonSheepzsObject>(json);

            GameObject go;
            if (parent == null)
            {
                go = Instantiate(GetPrefabForPolarity(obj.Polarity));
            }
            else
            {
                go = Instantiate(GetPrefabForPolarity(obj.Polarity), parent) as GameObject;
            }

            var so = go.GetComponent<SpaceObject>();
            so.CopyValues(obj);
            return so;
        }

        public GameObject GetPrefabForPolarity(Polarity polarity)
        {
            switch (polarity)
            {
                case Polarity.Negative:
                {
                    return NegativePolarizedObstacle;
                }
                case Polarity.Positive:
                {
                    return PositivePolarizedObstacle;
                }
                case Polarity.Neutral:
                    return NeutralPolarizedObstacle;
                default:
                    return null;
            }
        }
    }
}