﻿using UnityEngine;
using System.Collections;
using System;
using Newtonsoft.Json;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Codevember16
{
    public class SheepEvent : UnityEvent<GameObject, GameObject>
    {
    }

    public class FlyingSheepComponent : SpaceObject
    {
        [JsonIgnore] public SheepEvent OnSpaceObjectHitEvent = new SheepEvent();
        [JsonIgnore] public GameObject ArrowPrefab;
        [JsonIgnore] public GameObject Sternchen;
        [JsonIgnore] public GameObject LooksLikeWolve;
        [JsonIgnore] public GameObject LooksLikeSheep;
        [JsonIgnore] public GameObject CameraSlider;
        [JsonIgnore] public GameObject PlayerSheep;
        [JsonIgnore] public GameObject PlayerSheepWolf;
        [JsonIgnore] public GameObject PlayerSheepSheep;
        [JsonIgnore] public GameObject SternchenWrapper;

        private GameObject _arrowGo;
        private GameObject _tempStern;
        [HideInInspector] public Vector3 IniPosition;
        private Vector3 inputStart;
        private Vector3 _inputEnd;
        private bool _alreadyShot = false;

        private float _screenDiagonal;

        // defines the offset of the boundaries (on top of the level size)
        private float _boundaryOffset = 300f;

        // Simulation parameters
        public int SheepIdx;
        public int NumSheepInteractions = 0;
        public int FrameOfCreation;

        void Awake()
        {
            IniPosition = this.transform.position;

            if (GameLogic.Instance.IsSimulation)
            {
                Gravity.StaticOnTriggerEnterEvent.AddListener((grav, spaceObj) =>
                {
                    if (spaceObj == this)
                    {
                        NumSheepInteractions++;
                    }
                });
            }

            float pythagoras = (float) (Math.Pow(Screen.width, 2) + Math.Pow(Screen.height, 2));
            _screenDiagonal = (float) Math.Sqrt(pythagoras);
        }

        void Start()
        {
            if (DontDestroy.WasWolf)
            {
                this.Polarity = Polarity.Negative;
                //Debug.Log("pol: negative");
            }
            else
            {
                this.Polarity = Polarity.Positive;
                //Debug.Log("pol: pos");
            }
        }


        // Update is called once per frame
        void Update()
        {
            CheckTouchInput();
            CheckMouseInput();

            if (!FindObjectOfType<GameLogic>().IsSimulation)
            {
                // check if the sheep is out of the game area
                if (this.GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition.x >
                    FindObjectOfType<LevelController>().Dimensions.x + _boundaryOffset ||
                    this.GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition.y >
                    FindObjectOfType<LevelController>().Dimensions.y + _boundaryOffset ||
                    this.GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition.x < -_boundaryOffset ||
                    this.GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition.y < -_boundaryOffset)
                {
                    //TODO:
                    // Debug.Log("TOT!");
                }
            }
            else
            {
                if (
                    !(this.transform.position.x > FindObjectOfType<LevelGrid>().BottomLeft.x &&
                      this.transform.position.z > FindObjectOfType<LevelGrid>().BottomLeft.y &&
                      this.transform.position.x < FindObjectOfType<LevelGrid>().TopRight.x &&
                      this.transform.position.z < FindObjectOfType<LevelGrid>().TopRight.y) ||
                    Time.renderedFrameCount - FrameOfCreation > 500
                )
                {
                    Destroy(this.gameObject);
                }
            }

            if ((GetComponent<Rigidbody>().velocity).magnitude != 0.0f)
                transform.forward = GetComponent<Rigidbody>().velocity;
        }

        private void InstantiateArrow()
        {
            _arrowGo = Instantiate(ArrowPrefab, this.gameObject.transform, true) as GameObject;
            _arrowGo.transform.position += Vector3.back * 10;

            if (LooksLikeSheep.activeSelf)
                LooksLikeSheep.GetComponent<Button>().interactable = false;
            if (LooksLikeWolve.activeSelf)
                LooksLikeWolve.GetComponent<Button>().interactable = false;
            CameraSlider.GetComponent<Slider>().interactable = false;
            GameObject.Find("Menu").GetComponent<Button>().interactable = false;
        }

        private void ModifyArrow(Vector3 currentTouchPoint)
        {
            // calculate angle
            Vector3 diff = currentTouchPoint - inputStart;
            float shootPower = diff.magnitude / _screenDiagonal;
            if (shootPower > 0.5f) shootPower = 0.5f;
            Vector3 tempScale = new Vector3(0.02f, 0.001f, 0.05f);
            _arrowGo.transform.localScale = tempScale * shootPower * 300;

            // rotate arrow
            float angle = Vector3.Angle(diff, Vector3.up);
            // shoot to the left, if arrow points to the left
            if (diff.x < 0)
                angle *= -1.0f;
            _arrowGo.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        }

        private void RemoveArrow()
        {
            Destroy(_arrowGo);
        }

        private void CheckTouchInput()
        {
            //Touch Input
            if (Input.touchCount != 1)
            {
                return;
            }
            else
            {
                Touch touch = Input.touches[0];
                Vector3 touchPos = touch.position;

                if (touch.phase == TouchPhase.Began)
                {
                    //Debug.Log("touch");
                    if (Screen.width * 0.4f < touchPos.x &&
                        Screen.width * 0.6f > touchPos.x &&
                        Screen.height * 0.05f < touchPos.y &&
                        Screen.height * 0.23f > touchPos.y)
                    {
                        inputStart = touchPos;
                        if (!_alreadyShot)
                            InstantiateArrow();
                    }
                }

                if (touch.phase == TouchPhase.Moved)
                {
                    if (!_alreadyShot && _arrowGo)
                        ModifyArrow(touchPos);
                }

                if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
                    _inputEnd = touchPos;
                    if (!_alreadyShot && _arrowGo)
                    {
                        ShootObject();
                        _alreadyShot = true;
                        RemoveArrow();
                    }
                }
            }
        }

        private void CheckMouseInput()
        {
            if (Input.touchCount != 1)
            {
                var mousePos = Input.mousePosition;
                var refReso = this.GetComponent<GameObjectPositioner>().ReferenceResolution;

                if (Input.GetMouseButtonDown(0))
                {
                    if (refReso.x * 0.4f < mousePos.x &&
                        refReso.x * 0.6f > mousePos.x &&
                        refReso.y * 0.05f < mousePos.y &&
                        refReso.y * 0.23f > mousePos.y)
                    {
                        if (!_alreadyShot)
                            InstantiateArrow();
                        inputStart = Input.mousePosition;
                    }
                }

                if (Input.GetMouseButton(0))
                    if (!_alreadyShot && _arrowGo)
                        ModifyArrow(Input.mousePosition);

                if (Input.GetMouseButtonUp(0))
                {
                    _inputEnd = Input.mousePosition;

                    if (!_alreadyShot && _arrowGo)
                    {
                        ShootObject();
                        _alreadyShot = true;
                        RemoveArrow();
                    }
                }
            }
        }

        private void Hit(Vector3 position)
        {
            //Debug.Log("hit");
            Ray ray = Camera.main.ScreenPointToRay(position);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    //Debug.Log("hitted " + hit.transform.name);
                    ShootObject();
                }
            }
        }

        private void ShootObject()
        {
            if (!_alreadyShot)
            {
                // calculate angle
                Vector3 diff = _inputEnd - inputStart;
                float angle = Vector3.Angle(diff, Vector3.up);
                // shoot to the left, if arrow points to the left
                if (diff.x < 0)
                    angle *= -1.0f;

                // Scaling Factor for inputs 0-1
                float shootPower = diff.magnitude / _screenDiagonal;
                //Debug.Log (diff.magnitude / scaleFactor);
                //Debug.Log ("---");

                // half the screen should be max power
                if (shootPower > 0.5f)
                    shootPower = 0.5f;

                GetComponent<Rigidbody>()
                    .AddForce(Quaternion.Euler(0, angle, 0) * Vector3.forward * shootPower * 1000f);
                _alreadyShot = true;
            }
        }

        IEnumerator Remove(GameObject tempStern, float time)
        {
            yield return new WaitForSeconds(time);
            Destroy(tempStern);
        }

        void OnCollisionEnter(Collision col)
        {
            //Debug.Log("hit on dull object");
            _tempStern = Instantiate(Sternchen, SternchenWrapper.transform) as GameObject;
            _tempStern.transform.position = gameObject.transform.position;
            _tempStern.GetComponent<ParticleSystem>().Play();
            StartCoroutine(Remove(_tempStern, 1f));
        }

        void OnTriggerEnter(Collider col)
        {
            OnSpaceObjectHitEvent.Invoke(this.gameObject, col.transform.gameObject);
        }

        public IEnumerator Reset()
        {
            if (SternchenWrapper.transform.childCount > 0)
            {
                foreach (Transform s in SternchenWrapper.transform)
                {
                    Destroy(s.gameObject);
                }
            }

            if (LooksLikeSheep.activeSelf)
                LooksLikeSheep.GetComponent<Button>().interactable = true;
            if (LooksLikeWolve.activeSelf)
                LooksLikeWolve.GetComponent<Button>().interactable = true;
            CameraSlider.GetComponent<Slider>().interactable = true;

            if (_alreadyShot)
            {
                var playerSheep = Instantiate(PlayerSheep, gameObject.transform.parent, true) as GameObject;
                playerSheep.transform.position = IniPosition;
                playerSheep.transform.rotation = Quaternion.identity;
                // todo: there must be a better solution for this
                playerSheep.GetComponent<FlyingSheepComponent>().PlayerSheep = PlayerSheep;
                playerSheep.GetComponent<FlyingSheepComponent>().LooksLikeWolve = LooksLikeWolve;
                playerSheep.GetComponent<FlyingSheepComponent>().LooksLikeSheep = LooksLikeSheep;
                playerSheep.GetComponent<FlyingSheepComponent>().CameraSlider = CameraSlider;
                playerSheep.GetComponent<FlyingSheepComponent>().SternchenWrapper = SternchenWrapper;
                playerSheep.GetComponent<FlyingSheepComponent>().Sternchen = Sternchen;

                if (this.PlayerSheepWolf.activeSelf)
                {
                    DontDestroy.WasWolf = true;
                    playerSheep.GetComponent<FlyingSheepComponent>().PlayerSheepSheep.SetActive(false);
                    playerSheep.GetComponent<FlyingSheepComponent>().PlayerSheepWolf.SetActive(true);
                    playerSheep.GetComponent<FlyingSheepComponent>().Polarity = Polarity.Negative;
                }

                _alreadyShot = false;

                yield return null;

                Destroy(this.gameObject);
            }

            yield return null;
        }
    }
}