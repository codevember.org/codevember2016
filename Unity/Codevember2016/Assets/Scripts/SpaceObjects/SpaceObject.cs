using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

namespace Codevember16
{
    public enum Polarity
    {
        Positive,
        Negative,
        Neutral,
        Count
    }

    public class JsonGravityObject
    {
        public float Range { get; set; }
        public float Mass { get; set; }

        public float StrengthOfAttraction { get; set; }

        public override string ToString()
        {
            return String.Format("Range={0}, Mass={1}, StrengthOfAttraction={2}", Range, Mass, StrengthOfAttraction);
        }
    }

    public class JsonSheepzsObject
    {
        [JsonIgnore] public SpaceObject CorrespondingObject;

        public string Name { get; set; }

        public Vector2 SheepzWorldPosition { get; set; }

        public JsonGravityObject Gravity { get; set; }

        public Polarity Polarity { get; set; }

        public bool IsAttractable { get; set; }
        public bool IsAttractive { get; set; }

        public Quaternion Rotation { get; set; }

        public override string ToString()
        {
            return String.Format("Name={0}, Posi={1}, Polarity={2}, IsAttractable={3}, IsAttrative={4}, Gravity={5}",
                Name, SheepzWorldPosition, Polarity, IsAttractable, IsAttractive,
                Gravity == null ? "NA" : Gravity.ToString());
        }
    }

    public class JsonPascalCasePropertyAndFieldConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            // this converter can be applied to any type
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            // we currently support only writing of JSON
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            // Fix: Ignore loops
            serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            // Find all Properties written in PascalCasing.
            var properties = value.GetType().GetProperties().Where(p =>
                p.GetCustomAttributes(typeof(JsonIgnoreAttribute), false).Length == 0 &&
                !p.Name[0].ToString().ToLower().Equals(p.Name[0].ToString()));

            var fields = value.GetType().GetFields().Where(p =>
                p.GetCustomAttributes(typeof(JsonIgnoreAttribute), false).Length == 0 &&
                !p.Name[0].ToString().ToLower().Equals(p.Name[0].ToString()));
            writer.WriteStartObject();

            foreach (var property in properties)
            {
                // write property name
                writer.WritePropertyName(property.Name);
                // let the serializer serialize the value itself
                // (so this converter will work with any other type, not just int)
                serializer.Serialize(writer, property.GetValue(value, null));
            }

            foreach (var field in fields)
            {
                writer.WritePropertyName(field.Name);
                serializer.Serialize(writer, field.GetValue(value));
            }

            writer.WriteEndObject();
        }
    }

    [RequireComponent(typeof(GameObjectPositioner))]
    [JsonConverter(typeof(JsonPascalCasePropertyAndFieldConverter))]
    public class SpaceObject : MonoBehaviour
    {
        public bool IsAttractable;

        public bool IsAttractive;

        public Polarity Polarity;

        public Quaternion Rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }

        [JsonIgnore] public GameObject GravityGO;

        //[JsonIgnore]
        public Gravity Gravity
        {
            get
            {
                if (GravityGO != null)
                {
                    return GravityGO.GetComponent<Gravity>();
                }
                else
                {
                    return GetComponentInChildren<Gravity>();
                }
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private GameObjectPositioner _positioner;

        [HideInInspector]
        [JsonIgnore]
        public GameObjectPositioner Positioner
        {
            get
            {
                if (_positioner == null)
                {
                    _positioner = this.GetComponent<GameObjectPositioner>();
                }

                return _positioner;
            }
        }

        public Vector2 SheepzWorldPosition
        {
            get { return Positioner.SheepzWorldSpacePosition; }
            set { Positioner.SheepzWorldSpacePosition = value; }
        }

        protected virtual void Awake()
        {
        }

        public void CopyValues(JsonSheepzsObject json)
        {
            // Debug.Log(this.name + " copies values from:" + json.Name + " (" + Time.frameCount+")");
            this.Name = json.Name;
            this.IsAttractable = json.IsAttractable;
            this.IsAttractive = json.IsAttractive;
            this.Polarity = json.Polarity;
            this.SheepzWorldPosition = json.SheepzWorldPosition;
            this.transform.rotation = json.Rotation;
            if (json.Gravity != null)
            {
                this.Gravity.Mass = json.Gravity.Mass;
                this.Gravity.Range = json.Gravity.Range;
                this.Gravity.StrengthOfAttraction = json.Gravity.StrengthOfAttraction;
            }
        }
    }
}