﻿using UnityEngine;
using System;
using System.Collections;

namespace Codevember16
{
    public class RotateTowardsTarget : MonoBehaviour
    {
        [SerializeField]
        public GameObject Target
        {
            get
            {
                if (_target == null)
                {
                    _target = GameObject.FindObjectOfType<FlyingSheepComponent>().gameObject;
                }

                return _target;
            }
            set { _target = value; }
        }

        private GameObject _target;
        public float speed = 0.75f;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 targetDir = Target.transform.position - transform.position;
            float step = speed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
            Debug.DrawRay(transform.position, newDir, Color.red);
            transform.rotation = Quaternion.LookRotation(newDir);
        }
    }
}