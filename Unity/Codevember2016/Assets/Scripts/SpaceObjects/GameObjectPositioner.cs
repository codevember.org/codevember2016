﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Prosodiya;
using UnityEngine;


namespace Codevember16
{
    [ExecuteInEditMode]
    /// <summary>
    /// Positions a game object in the game world by converting the given "WorldCoordinates", bases on the reference resolutuon, into the PPU Unity position.
    /// </summary>
    public class GameObjectPositioner : MonoBehaviour
    {
        [SerializeField] protected Vector2 _sheepzWorldSpacePosition;

        public Vector2 SheepzWorldSpacePosition
        {
            get { return _sheepzWorldSpacePosition; }
            set
            {
                if (_sheepzWorldSpacePosition != value)
                {
                    _sheepzWorldSpacePosition = value;
                    this.UpdatePosition();
                }
            }
        }

        public Vector2 ReferenceResolution
        {
            get { return Settings.ReferenceResolution; }
        }

        public int ReferencePPU
        {
            get { return Settings.ReferencePPU; }
        }

        private float previousZ;

        [Tooltip(
            "If set, the GameObjectPositioner will start with the values given in the transform. Any later changes to the ProsodiyaWorldSpacePosition will trigger UpdatePosition.")]
        public bool StartWithTransformValues = false;

        private Vector2 _previousProsodiyaWorldSpacePos;

        void Start()
        {
            if (StartWithTransformValues)
            {
                //Debug.Log("StartWithTansform: " + this.transform.ToSheepzWorldSpace());
                _sheepzWorldSpacePosition = this.transform.ToSheepzWorldSpace();
            }

            previousZ = this.transform.position.z;
            _previousProsodiyaWorldSpacePos = _sheepzWorldSpacePosition;
        }

        void Update()
        {
            if (_sheepzWorldSpacePosition != this.transform.ToSheepzWorldSpace().XZ())
            {
                this.SheepzWorldSpacePosition = this.transform.ToSheepzWorldSpace().XZ();
            }
        }

        public void UpdatePosition()
        {
            if (this.transform.ToSheepzWorldSpace().XZ().Equals(_sheepzWorldSpacePosition))
            {
                return;
            }

            //Debug.Log(this.name + " update pos:" + this.transform.ToSheepzWorldSpace().XZ() + " -> " + _sheepzWorldSpacePosition);
            var vec = this.ReferenceResolution;
            var targetPos = (this._sheepzWorldSpacePosition - vec / 2) / ReferencePPU;
            this.transform.position = new Vector3(targetPos.x, this.transform.position.y, targetPos.y);
        }


        public static Vector2 TransformToUnityPosition(Vector2 positionInWorldCoordinates,
            Vector2 referenceResolution = default(Vector2), int referencePPU = 100)
        {
            if (referenceResolution.Equals(default(Vector2)))
            {
                referenceResolution = new Vector2(2048, 1536);
            }

            return (positionInWorldCoordinates - referenceResolution / 2) / referencePPU;
        }
    }
}