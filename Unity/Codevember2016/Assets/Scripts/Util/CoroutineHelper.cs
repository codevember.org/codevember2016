using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Prosodiya;
using UnityEngine;

namespace Codevember16
{
    public class CoroutineHelper : MonoBehaviourSingleton<CoroutineHelper>
    {
        public TType[] FindObjectsOfTypeHelper<TType>() where TType : UnityEngine.Object
        {
            return FindObjectsOfType<TType>();
        }

        public GameObject InstantiateHelper(GameObject gameObject, Transform parent = null,
            bool worldPositionStays = true)
        {
            if (parent != null)
            {
                return Instantiate(gameObject, parent, worldPositionStays) as GameObject;
            }
            else
            {
                return Instantiate(gameObject);
            }
        }
    }
}