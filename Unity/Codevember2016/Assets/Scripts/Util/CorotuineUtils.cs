using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Codevember16
{
    public static class CoroutineUtils
    {
        /// <summary>
        /// Starts the coroutine.
        /// </summary>
        /// <param name="co">The co.</param>
        /// <returns></returns>
        public static Coroutine StartCoroutine(IEnumerator co)
        {
            return CoroutineHelper.Instance.StartCoroutine(co);
        }

        /// <summary>
        /// Starts the coroutine.
        /// </summary>
        /// <param name="co">The co.</param>
        /// <returns></returns>
        public static Coroutine StartCoroutine(string co)
        {
            return CoroutineHelper.Instance.StartCoroutine(co);
        }

        /// <summary>
        /// Stops the coroutine.
        /// </summary>
        /// <param name="co">The co.</param>
        public static void StopCoroutine(IEnumerator co)
        {
            CoroutineHelper.Instance.StopCoroutine(co);
        }

        /**
         * Usage: StartCoroutine(CoroutineUtils.Chain(...))
         * For example:
         *     StartCoroutine(CoroutineUtils.Chain(
         *         CoroutineUtils.Do(() => Debug.Log("A")),
         *         CoroutineUtils.WaitForSeconds(2),
         *         CoroutineUtils.Do(() => Debug.Log("B"))));
         */
        public static IEnumerator Chain(params IEnumerator[] actions)
        {
            foreach (var action in actions)
            {
                yield return CoroutineHelper.Instance.StartCoroutine(action);
            }
        }

        /**
         * Usage: StartCoroutine(CoroutineUtils.DelaySeconds(action, delay))
         * For example:
         *     StartCoroutine(CoroutineUtils.DelaySeconds(
         *         () => DebugUtils.Log("2 seconds past"),
         *         2);
         */
        public static IEnumerator DelaySeconds(Action action, float delay)
        {
            yield return new WaitForSeconds(delay);
            action();
        }

        public static IEnumerator WaitForSeconds(float time)
        {
            yield return new WaitForSeconds(time);
        }

        public static IEnumerator WaitForCondition(Func<bool> condition)
        {
            //Debug.Log("Start wait for cond");
            yield return null;
            while (!condition())
            {
                yield return new WaitForSeconds(0.01f);
            }

            //Debug.Log("End wait for cond");
        }

        public static IEnumerator WaitForConditionTyped<TType>(Func<TType, bool> condition, TType executer)
        {
            if (condition(executer))
            {
                yield return null;
            }
            else
            {
                while (!condition(executer))
                {
                    yield return new WaitForSeconds(0.01f);
                }
            }
        }

        public static IEnumerator Do(Action action)
        {
            action();
            yield return null;
        }

        public static IEnumerator Do(Action action, float time)
        {
            action();
            yield return new WaitForSeconds(time);
        }

        public static IEnumerator Do(Action action, Func<bool> waitCondition)
        {
            action();
            yield return WaitForCondition(waitCondition);
        }

        public static T[] FindObjectsOfType<T>() where T : MonoBehaviour
        {
            return CoroutineHelper.Instance.FindObjectsOfTypeHelper<T>();
        }

        public static T FindObjectOfType<T>() where T : MonoBehaviour
        {
            return CoroutineHelper.Instance.FindObjectsOfTypeHelper<T>().First();
        }

        public static GameObject Instantiate(GameObject gameObject, Transform parent = null,
            bool worldPositionStays = true)
        {
            return CoroutineHelper.Instance.InstantiateHelper(gameObject, parent, worldPositionStays);
        }
    }
}