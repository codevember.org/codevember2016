﻿using UnityEngine;
using System.Collections;
using Codevember16;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine.UI;

public class GameLogic : MonoBehaviourSingleton<GameLogic>
{
    public GameObject LevelFinished;
    public GameObject bg;
    public Text LevelName;

    private int _numResets;
    public bool IsSimulation = false;

    public int CurrentLevelNum
    {
        get { return PlayerPrefs.GetInt("CurrentLevelNum"); }
        set { PlayerPrefs.SetInt("CurrentLevelNum", value); }
    }

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        FindObjectOfType<FlyingSheepComponent>().OnSpaceObjectHitEvent.AddListener(OnCollision);

        _numResets = 0;
        LevelFinished = GameObject.Find("Popup");
        bg = GameObject.Find("Background");
    }

    void OnCollision(GameObject flyingGo, GameObject hittedGo)
    {
        //Debug.Log(flyingGo + " hitted " + hittedGo);
        if (hittedGo.name.Equals("Goal"))
        {
            ScoreCounter.Instance.AwardScore(ScoreCounter.EScoreType.FullLevel, _numResets);
            LevelFinished.SetActive(true);
            LevelFinished.GetComponent<CanvasGroup>().alpha = 1.0f;

            GameObject.Find("Menu").GetComponent<Button>().interactable = false;

            bg.SetActive(true);
            bg.GetComponent<CanvasGroup>().alpha = 1.0f;

            GameObject.Find("PopUpWindow/Text").GetComponent<UnityEngine.UI.Text>().text =
                ScoreCounter.Instance.Points.ToString();
        }
    }

    public IEnumerator ResetCurrentLevel()
    {
        _numResets++;
        //CameraController.Instance.Reset();
        StartCoroutine(FindObjectOfType<FlyingSheepComponent>().Reset());
        GameObject.Find("Menu").GetComponent<Button>().interactable = true;

        yield return null;
        yield return null;
        yield return null;

        FindObjectOfType<FlyingSheepComponent>().OnSpaceObjectHitEvent.AddListener(OnCollision);
    }


    public void StartNextLevel()
    {
        // Debug.Log("Start Next Level : " + CurrentLevelNum + " -> " + (CurrentLevelNum+1));
        //StartCoroutine(FindObjectOfType<FlyingSheepComponent>().Reset());
        StartCoroutine(ResetCurrentLevel());
        FindObjectOfType<LevelController>().ClearLevelFromObstacles();
        var lvls = Extensions.LoadLevelList();
        //foreach (var l in lvls)
        //{
        //    Debug.Log(l.name);
        //}
        // Debug.Log("Levels.Count:" + lvls.Count);

        if (CurrentLevelNum + 1 > lvls.Count - 1)
        {
            Debug.Log("finished game!");
            GameObject.Find("Canvas").GetComponent<FinishMessage>().OpenFinishBox();
        }
        else
        {
            var nextLevel = lvls[Mathf.Min(CurrentLevelNum++, lvls.Count - 1)];
            //Debug.Log("Start Level:" + nextLevel.name);
            LoadLevelFromTextAsset(nextLevel);

            GameObject.Find("Menu").GetComponent<Button>().interactable = true;

            _numResets = 0;
            LevelName.text = nextLevel.name.Replace("_", " ");
        }
    }

    public void ResetCurrentLevelNum()
    {
        CurrentLevelNum = 0;
    }

    public static void LoadLevelFromTextAsset(TextAsset textAsset)
    {
        var deserialize = JsonConvert.DeserializeObject<Level>(textAsset.text);

        // Load the sheep.
        FindObjectOfType<LevelController>().Sheep.CopyValues(deserialize.Sheep);

        foreach (var so in deserialize.Obstacles)
        {
            FindObjectOfType<ObstacleFactory>().CreateSheepzObjectFromJson(so,
                FindObjectOfType<LevelController>().ObstaclesContainer.transform);
        }
    }
}