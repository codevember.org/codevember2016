﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using Codevember16;

public class ButtonsStartScene : MonoBehaviour
{
    public GameObject ButtonLevelPrefab;
    public GameObject LevelWrapper;

    private List<GameObject> lvlsForSelection = new List<GameObject>();

    private AudioSource sound;
    private GameObject buttonWrapper;
    private GameObject story;
    private GameObject explanation;
    private GameObject dressOrNot;
    private GameObject highscorePanel;

    private GameObject highscore;
    //private GameObject levelSelection;

    void Awake()
    {
        //PlayerPrefs.DeleteAll();
    }

    void Start()
    {
        buttonWrapper = GameObject.Find("ButtonWrapper");
        story = GameObject.Find("TextPanel");
        explanation = GameObject.Find("Explanation");
        dressOrNot = GameObject.Find("Alternative");
        highscorePanel = GameObject.Find("HighscorePanel");
        highscore = GameObject.Find("Highscore");
        ;
        //levelSelection = GameObject.Find("LevelSelection");


        //levelSelection.GetComponent<CanvasGroup>().alpha = 0.0f;
        //levelSelection.SetActive(false);

        story.GetComponent<CanvasGroup>().alpha = 0.0f;
        story.SetActive(false);

        explanation.GetComponent<CanvasGroup>().alpha = 0.0f;
        explanation.SetActive(false);

        dressOrNot.GetComponent<CanvasGroup>().alpha = 0.0f;
        dressOrNot.SetActive(false);

        highscorePanel.GetComponent<CanvasGroup>().alpha = 0.0f;
        highscorePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }

    // Start-Button was pressed: Starts the game
    public void startPressed(string wayOfStarting)
    {
        if (wayOfStarting.Equals("Start"))
        {
            SceneManager.LoadScene("PlayScene");
        }

        if (wayOfStarting.Equals("Help"))
        {
            PlayerPrefs.SetInt("Already Played", 1);
            SceneManager.LoadScene("PlayScene");
        }

        ScoreCounter.Instance.ResetScore();
    }

    // Settings-Button was pressed: Settingsscreen is opened
    public void highscorePressed()
    {
        // Debug.Log("Highscore was pressed");

        highscore.GetComponent<Text>().text = PlayerPrefs.GetInt("HIGHSCORE").ToString();

        highscorePanel.SetActive(true);
        highscorePanel.GetComponent<CanvasGroup>().alpha = 1.0f;
    }

    public void startTutorial()
    {
        // Debug.Log("Tutorial was pressed");

        buttonWrapper.GetComponent<CanvasGroup>().alpha = 0.0f;
        buttonWrapper.SetActive(false);

        story.SetActive(true);
        story.GetComponent<CanvasGroup>().alpha = 1.0f;
    }

    // GoOn-Button was pressed: next Tutorial-page is displyed
    public void goOnTutorial(string obj)
    {
        if (obj.Equals("GoOn1"))
        {
            story.GetComponent<CanvasGroup>().alpha = 0.0f;
            story.SetActive(false);

            explanation.SetActive(true);
            explanation.GetComponent<CanvasGroup>().alpha = 1.0f;
        }

        if (obj.Equals("GoOn2"))
        {
            explanation.GetComponent<CanvasGroup>().alpha = 0.0f;
            explanation.SetActive(false);

            dressOrNot.SetActive(true);
            dressOrNot.GetComponent<CanvasGroup>().alpha = 1.0f;
        }
    }

    // GoOn-Button was pressed: Back to StartScreen
    public void mainMenu(bool highscore)
    {
        if (highscore)
        {
            highscorePanel.GetComponent<CanvasGroup>().alpha = 0.0f;
            highscorePanel.SetActive(false);
        }
        else
        {
            dressOrNot.GetComponent<CanvasGroup>().alpha = 0.0f;
            dressOrNot.SetActive(false);

            buttonWrapper.SetActive(true);
            buttonWrapper.GetComponent<CanvasGroup>().alpha = 1.0f;
        }
    }

    // Exit-Button was pressed: Game exits to homescreen
    public void exitPressed()
    {
        // Debug.Log("Game exits now and changes to Homescreen");
        Application.Quit();
        //SelectLevel();
    }

    // Music-Mute-Button was pressed: Music is muted
    public void musicMuted()
    {
        sound = GameObject.Find("Sound").GetComponent<AudioSource>();

        if (sound.mute)
        {
            sound.mute = false;
        }
        else
        {
            sound.mute = true;
        }
    }

    public void SelectLevel()
    {
        buttonWrapper.GetComponent<CanvasGroup>().alpha = 0.0f;
        buttonWrapper.SetActive(false);

        //levelSelection.SetActive(true);
        //levelSelection.GetComponent<CanvasGroup>().alpha = 1.0f;

        var lvlList = Extensions.LoadLevelList();
        var counter = 0;
        foreach (var lvl in lvlList)
        {
            var lvlGo = Instantiate(ButtonLevelPrefab, LevelWrapper.transform, false) as GameObject;
            lvlGo.GetComponentInChildren<Text>().text = (++counter).ToString();
            lvlGo.name = lvl.name;
            lvlGo.GetComponent<LevelButtonScript>().LevelAsset = lvl;
            lvlsForSelection.Add(lvlGo);
        }
    }

    public void LevelSelectionBack()
    {
        //levelSelection.GetComponent<CanvasGroup>().alpha = 0.0f;
        //levelSelection.SetActive(false);

        buttonWrapper.SetActive(true);
        buttonWrapper.GetComponent<CanvasGroup>().alpha = 1.0f;

        //Cleanup Levelselection
        foreach (var v in lvlsForSelection)
        {
            Destroy(v);
        }

        lvlsForSelection.Clear();
    }
}