﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Codevember16
{
    public class FinishMessage : MonoBehaviour
    {
        public GameObject MsgBox;
        public Text Highscore;
        public GameObject Background;
        private AudioSource ASource;

        // Use this for initialization
        private void Awake()
        {
            MsgBox.SetActive(false);
            ASource = GameObject.Find("Sound").GetComponent<AudioSource>();
        }

        // Update is called once per frame
        public void OpenFinishBox()
        {
            Background.SetActive(true);
            Background.GetComponent<CanvasGroup>().alpha = 1.0f;
            MsgBox.SetActive(true);
            Highscore.text = ScoreCounter.Instance.Points.ToString();
            ASource.clip = (AudioClip) Resources.Load("win");
            ASource.loop = false;
            ASource.Play();
        }
    }
}