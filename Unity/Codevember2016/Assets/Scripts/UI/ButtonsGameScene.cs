﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Codevember16
{
    public class ButtonsGameScene : MonoBehaviour
    {
        public static UnityEvent OnPolarityChangeEvent = new UnityEvent();

        private AudioSource sound;
        private AudioClip gameSound;
        private GameObject bg;
        private GameObject popUp;
        private GameObject buttonWrappper;
        private GameObject gameMenu;
        private GameObject help1;
        private GameObject help2;
        private GameObject help3;
        private GameObject helpNotStart1;
        private GameObject helpNotStart2;
        private GameObject helpNotStart3;
        public GameObject looksLikeWolf;
        public GameObject looksLikeSheep;

        // Use this for initialization
        void Start()
        {
            sound = GameObject.Find("Sound").GetComponent<AudioSource>();
            gameSound = (AudioClip) Resources.Load("cv3_game_buildup_1");
            bg = GameObject.Find("Background");
            popUp = GameObject.Find("Popup");
            buttonWrappper = GameObject.Find("ButtonWrapper");
            gameMenu = GameObject.Find("GameMenu");
            help1 = GameObject.Find("StoryText");
            help2 = GameObject.Find("HowToPlay");
            help3 = GameObject.Find("Choice");
            helpNotStart1 = GameObject.Find("StoryTextInHelp");
            helpNotStart2 = GameObject.Find("HowToPlayInHelp");
            helpNotStart3 = GameObject.Find("ChoiceInHelp");
            //looksLikeWolf = GameObject.Find("PolarityWolf");
            //looksLikeSheep = GameObject.Find("PolaritySheep");

            if (sound.mute)
            {
                sound.mute = true;
            }

            sound.clip = gameSound;
            sound.Play();

            bg.GetComponent<CanvasGroup>().alpha = 0.0f;
            bg.SetActive(false);

            popUp.GetComponent<CanvasGroup>().alpha = 0.0f;
            popUp.SetActive(false);

            gameMenu.GetComponent<CanvasGroup>().alpha = 0.0f;
            gameMenu.SetActive(false);

            help1.GetComponent<CanvasGroup>().alpha = 0.0f;
            help1.SetActive(false);

            help2.GetComponent<CanvasGroup>().alpha = 0.0f;
            help2.SetActive(false);

            help3.GetComponent<CanvasGroup>().alpha = 0.0f;
            help3.SetActive(false);

            helpNotStart1.GetComponent<CanvasGroup>().alpha = 0.0f;
            helpNotStart1.SetActive(false);

            helpNotStart2.GetComponent<CanvasGroup>().alpha = 0.0f;
            helpNotStart2.SetActive(false);

            helpNotStart3.GetComponent<CanvasGroup>().alpha = 0.0f;
            helpNotStart3.SetActive(false);

            looksLikeSheep.GetComponent<CanvasGroup>().alpha = 0.0f;
            looksLikeSheep.SetActive(false);

            if (PlayerPrefs.GetInt("Already Played") == 0)
            {
                showHelp(1);
            }
        }

        // Update is called once per frame
        //void Update()
        //{

        //    if (Input.GetKeyDown(KeyCode.RightArrow))
        //    {
        //        popUp.GetComponent<CanvasGroup>().alpha = 1.0f;
        //        popUp.SetActive(true);
        //    }
        //}

        // Menu-Button was pressed: menu opens
        public void openMenu()
        {
            if (gameMenu.activeSelf)
            {
                gameMenu.GetComponent<CanvasGroup>().alpha = 0.0f;
                gameMenu.SetActive(false);

                bg.GetComponent<CanvasGroup>().alpha = 0.0f;
                bg.SetActive(false);
            }
            else
            {
                gameMenu.SetActive(true);
                gameMenu.GetComponent<CanvasGroup>().alpha = 1.0f;

                bg.SetActive(true);
                bg.GetComponent<CanvasGroup>().alpha = 1.0f;
            }
        }

        // Restart-Button was pressed: Level starts again
        public void restartLevel()
        {
            gameMenu.SetActive(true);
            StartCoroutine(GameLogic.Instance.ResetCurrentLevel());

            gameMenu.GetComponent<CanvasGroup>().alpha = 0.0f;
            //gameMenu.SetActive(false);

            bg.GetComponent<CanvasGroup>().alpha = 0.0f;
            bg.SetActive(false);
        }

        // Cancel-Button was pressed: returns to GameScreen
        public void cancelMenu()
        {
            gameMenu.GetComponent<CanvasGroup>().alpha = 0.0f;
            gameMenu.SetActive(false);

            bg.GetComponent<CanvasGroup>().alpha = 0.0f;
            bg.SetActive(false);
        }

        // Menu-Button was pressed: GameScreen goes back to StartScreen
        public void backToMenu()
        {
            SceneManager.LoadScene("StartScreen");
        }

        // Next-Level-Button was pressed: Starts next Level
        public void nextLevel()
        {
            popUp.GetComponent<CanvasGroup>().alpha = 0.0f;
            popUp.SetActive(false);

            bg.GetComponent<CanvasGroup>().alpha = 0.0f;
            bg.SetActive(false);

            FindObjectOfType<GameLogic>().StartNextLevel();
        }

        // Music-Mute-Button was pressed: Music is muted
        public void musicMuted()
        {
            if (sound.mute)
            {
                sound.mute = false;
            }
            else
            {
                sound.mute = true;
            }
        }

        // Change-Polarity-Button was pressed: The Polarity of Whooley changes
        public void changePolatity(bool isSheep)
        {
            OnPolarityChangeEvent.Invoke();

            //Debug.Log(isSheep);
            //Debug.Log("Polarity should be changed now");
            if (isSheep)
            {
                looksLikeSheep.GetComponent<CanvasGroup>().alpha = 0.0f;
                looksLikeSheep.SetActive(false);

                looksLikeWolf.SetActive(true);
                looksLikeWolf.GetComponent<CanvasGroup>().alpha = 1.0f;
            }
            else
            {
                looksLikeWolf.GetComponent<CanvasGroup>().alpha = 0.0f;
                looksLikeWolf.SetActive(false);

                looksLikeSheep.SetActive(true);
                looksLikeSheep.GetComponent<CanvasGroup>().alpha = 1.0f;
            }
        }

        // Game ist played for the first time: Help is displayed
        public void showHelp(int whichHelp)
        {
            switch (whichHelp)
            {
                case 1:
                    help1.SetActive(true);
                    help1.GetComponent<CanvasGroup>().alpha = 1.0f;
                    break;
                case 2:
                    help1.GetComponent<CanvasGroup>().alpha = 0.0f;
                    help1.SetActive(false);

                    help2.SetActive(true);
                    help2.GetComponent<CanvasGroup>().alpha = 1.0f;
                    break;
                case 3:
                    help2.GetComponent<CanvasGroup>().alpha = 0.0f;
                    help2.SetActive(false);

                    help3.SetActive(true);
                    help3.GetComponent<CanvasGroup>().alpha = 1.0f;
                    break;
                case 4:
                    help3.GetComponent<CanvasGroup>().alpha = 0.0f;
                    help3.SetActive(false);

                    // Sets Flag that the Game was already played
                    PlayerPrefs.SetInt("Already Played", 1);
                    break;
                case 5:
                    helpNotStart1.SetActive(true);
                    helpNotStart1.GetComponent<CanvasGroup>().alpha = 1.0f;
                    break;
                case 6:
                    helpNotStart1.GetComponent<CanvasGroup>().alpha = 0.0f;
                    helpNotStart1.SetActive(false);

                    helpNotStart2.SetActive(true);
                    helpNotStart2.GetComponent<CanvasGroup>().alpha = 1.0f;
                    break;
                case 7:
                    helpNotStart2.GetComponent<CanvasGroup>().alpha = 0.0f;
                    helpNotStart2.SetActive(false);

                    helpNotStart3.SetActive(true);
                    helpNotStart3.GetComponent<CanvasGroup>().alpha = 1.0f;
                    break;
                case 8:
                    helpNotStart3.GetComponent<CanvasGroup>().alpha = 0.0f;
                    helpNotStart3.SetActive(false);
                    break;
                default:
                    break;
            }
        }
    }
}