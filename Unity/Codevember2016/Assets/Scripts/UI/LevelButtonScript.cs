﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Codevember16
{
    public class LevelButtonScript : MonoBehaviour
    {
        public TextAsset LevelAsset;

        public void StartLevel()
        {
            SceneManager.LoadScene("PlayScene");
            GameLogic.LoadLevelFromTextAsset(LevelAsset);
        }
    }
}