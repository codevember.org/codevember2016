﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Level;
using Newtonsoft.Json;
using Random = System.Random;
using System.IO;


namespace Codevember16.LevelSimulation
{
    public class LevelSimulation : MonoBehaviourSingleton<LevelSimulation>
    {
        private System.Random _rnd = new System.Random();
        public GameObject BulletPrefab;
        public int BatchSize = 10;
        public int SimulationSize = 60;
        private int _bulletRunningIdx = 0;
        private float _directionSteps;
        public float ShootingAngle = 160;

        private Vector2 _goalIniPosition;

        public List<GameObject> Obstacles = new List<GameObject>();

        public Vector2 GravitySphereRadiusRange = new Vector2(1f, 2.5f);
        public Vector2 GravityAttractionStrengthRange = new Vector2(1f, 2.5f);
        public Vector2 MassRange = new Vector2(1f, 20f);

        public TextAsset FileAsset;

        // Use this for initialization
        IEnumerator Start()
        {
//            FileAsset = Resources.Load<TextAsset>("Levels/Level_1_Lea");
//
//            if (FileAsset != null)
//            {
//                LoadSimulationSceneFromFile();
//            }
//            else
//            {
//                FindObjectOfType<ObstacleFactory>().ObstaclesCount = 6;
//                FindObjectOfType<ObstacleFactory>().GenerateRandomObstacles();
//            }
            // initialise the grid boundaries
            var grid = FindObjectOfType<LevelGrid>();
            grid.BottomLeft = new Vector2(-grid.transform.position.x, grid.transform.position.z);
            grid.TopRight = new Vector2(grid.transform.position.x,
                2 * (grid.transform.position.z + grid.GetComponent<RectTransform>().sizeDelta.y));
            _directionSteps = ShootingAngle / System.Math.Max(SimulationSize - 1, 1);

            FindObjectOfType<GameLogic>().IsSimulation = true;

            _goalIniPosition =
                FindObjectOfType<GoalComponent>().GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition;
            FindObjectOfType<GoalComponent>().GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition =
                new Vector2(-1000, -1000);

            yield return CreateBullets();
        }

        // Update is called once per frame
        void Update()
        {
            StartCoroutine(CreateBullets());
        }

        public IEnumerator CreateBullets()
        {
            yield return null;
            var bulletLst = FindObjectsOfType<FlyingSheepComponent>();
            while (bulletLst.Length < BatchSize && SimulationSize > 0)
            {
                Debug.Log(String.Format("Creating bullet with idx {0}", _bulletRunningIdx));
                var bo = Instantiate(BulletPrefab) as GameObject;
                var bc = bo.GetComponent<FlyingSheepComponent>();
                bo.GetComponent<SphereCollider>().enabled = false;
                bc.FrameOfCreation = Time.renderedFrameCount;
                bc.SheepIdx = _bulletRunningIdx;
                _bulletRunningIdx++;
                bc.Positioner.SheepzWorldSpacePosition = new Vector2(
                    (int) Settings.ReferenceResolution.x / 2, 0);
                yield return null;

                float angle = _rnd.NextFloat(-ShootingAngle / 2, ShootingAngle / 2);
//                float angle = _directionSteps*bc.SheepIdx - ShootingAngle/2;
                float velocity = _rnd.NextFloat(20, 250);

                bc.GetComponent<Rigidbody>().AddForce(Quaternion.Euler(0, angle, 0) * Vector3.forward * velocity);
                bulletLst = FindObjectsOfType<FlyingSheepComponent>();

                yield return new WaitForSeconds(1);
                bo.GetComponent<SphereCollider>().enabled = true;

                SimulationSize--;
            }

            if (SimulationSize == 0)
            {
                FinaliseSimulation();
            }
        }

        private void FinaliseSimulation()
        {
            if (Time.timeScale == 0.0f)
            {
                return;
            }

            Debug.Log("Simulation finished, saving results");

            // Move the goal back to place
            FindObjectOfType<GoalComponent>().GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition =
                _goalIniPosition;
            // Get the Heatmap list
            var cells = FindObjectOfType<LevelGrid>().GetComponentsInChildren<CellObject>();

            // Find the maximumal interactions value for normalisation
            float maxInteractions = 0;
            for (int i = 0; i < FindObjectOfType<LevelGrid>().CellCount.x; i++)
            {
                var cellBulletsIds =
                    cells[
                        (int) FindObjectOfType<LevelGrid>().CellCount.x *
                        (int) FindObjectOfType<LevelGrid>().CellCount.y -
                        i -
                        1].EnteredObjects;
                cellBulletsIds = FindObjectOfType<VisualiseMesh>().InteractionFilter(cellBulletsIds);
                if (cellBulletsIds.Count > maxInteractions)
                {
                    maxInteractions = cellBulletsIds.Count;
                }
            }

            // CellCount.x is the number of cells along the x-axis => the number of cols
            int numRows = (int) FindObjectOfType<LevelGrid>().CellCount.y;
            int numCols = (int) FindObjectOfType<LevelGrid>().CellCount.x;

            FindObjectOfType<LevelController>().Level.HeatMap = new float[numRows, numCols];


            for (int c = 0; c < numCols; c++)
            {
                for (int r = 0; r < numRows; r++)
                {
                    int cellIdx = c + r * numCols;
                    FindObjectOfType<LevelController>().Level.HeatMap[r, c] =
                        System.Math.Min(cells[cellIdx].EnteredObjects.Count / (float) maxInteractions, 1.0f);
                }
            }

            // save level to file
            var lvl = FindObjectOfType<LevelController>().Level;
            Debug.Log(JsonConvert.SerializeObject(lvl, Formatting.Indented));

            //string path = EditorUtility.SaveFilePanel("Save Level", "Assets/Resources/Levels", "Sheepz", "sl");
            //if (path.Length != 0)
            //{
            //    File.WriteAllText(path, JsonConvert.SerializeObject(lvl, Formatting.Indented));
            //}

            Time.timeScale = 0.0f;
        }
    }
}