﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Codevember16.LevelSimulation;

public class DensityGrid : MonoBehaviour
{
    private int _resolutionX;
    private int _resolutionY;
    public List<int>[] _grid;
    private Matrix4x4 _toLocal;
    private Matrix4x4 _toWorld;
    private VisualiseMesh_Tobias _mesh;

    public int MaximumDensity
    {
        get { return _maximumDensity; }
    }

    private int _maximumDensity;

    // Use this for initialization
    void Awake()
    {
        GameObject floor = transform.Find("PlaneMesh").gameObject;
//		_mesh = floor.GetComponent<VisualiseMesh>();
        _resolutionX = 10;
        _resolutionY = 10;

        // init internal memory
        _grid = new List<int>[_resolutionX * _resolutionY];

        for (int i = 0; i < _resolutionX * _resolutionY; i++)
        {
            _grid[i] = new List<int>();
        }

        // get transformation relative to Floor
        _toLocal = floor.transform.worldToLocalMatrix;
        _toWorld = floor.transform.localToWorldMatrix;
    }

    // Update is called once per frame
    void Update()
    {
        Bullet[] activeBullets = Resources.FindObjectsOfTypeAll<Bullet>();

        foreach (Bullet bullet in activeBullets)
        {
            Vector3 pos = _toLocal * (bullet.transform.position - transform.position);
            Vector2 gridIndex = new Vector2((float) Math.Floor(pos.x * _resolutionX),
                (float) Math.Floor(pos.z * _resolutionY));

            //Debug.Log(String.Format("{0} -> {1}, {2}",bullet.transform.position, pos, gridIndex));

            if (_vectorInBounds(gridIndex) && gridIndex != bullet.LastGridIndex)
            {
                //Debug.Log (String.Format ("lastIndex {0}, new {1}", bullet.LastGridIndex, gridIndex));
                bullet.LastGridIndex = gridIndex;
                int index = (int) gridIndex.y * _resolutionX + (int) gridIndex.x;
                _grid[index].Add(bullet.MyIdx);

                _maximumDensity = Math.Max(_maximumDensity, _grid[index].Count);

                //Debug.Log (String.Format ("Update {0}: {1}", gridIndex, _grid [index]));
            }
        }
    }

    private bool _vectorInBounds(Vector2 idx)
    {
        int x = (int) idx.x;
        int y = (int) idx.y;
        return (x >= 0 && x < _resolutionX) && (y >= 0 && y < _resolutionY);
    }

    public List<int> GetIdsForIndex(Vector2 idx)
    {
        if (_grid == null)
        {
            return new List<int>();
        }

        Debug.Assert(_vectorInBounds(idx));
        int x = (int) idx.x;
        int y = (int) idx.y;
        return _grid[y * _resolutionX + x];
    }
}