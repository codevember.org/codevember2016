using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Codevember16;
using Codevember16.LevelSimulation;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class VisualiseMesh_Tobias : MonoBehaviourSingleton<VisualiseMesh_Tobias>
{
    public int XResolution, YResolution;

    public int MinInteractions = 0;
    public int MaxInteractions = 100;

    private Vector3[] _vertices;
    private Mesh _mesh;

    private Color32[] _heatmap = new Color32[]
    {
        new Color32(255, 245, 240, 100),
        new Color32(254, 224, 210, 120),
        new Color32(252, 187, 161, 140),
        new Color32(252, 146, 114, 160),
        new Color32(251, 106, 74, 180),
        new Color32(239, 59, 44, 200),
        new Color32(203, 24, 29, 220),
        new Color32(165, 15, 21, 240),
        new Color32(103, 0, 13, 255),
    };

    private void Awake()
    {
        Generate();
    }

    private void Generate()
    {
        GetComponent<MeshFilter>().mesh = _mesh = new Mesh();
        _mesh.name = "Procedural Grid";

        _vertices = new Vector3[(XResolution + 1) * (YResolution + 1)];
        for (int i = 0, y = 0; y <= YResolution; y++)
        {
            for (int x = 0; x <= XResolution; x++, i++)
            {
                _vertices[i] = new Vector3((float) x / XResolution, 0, (float) y / YResolution);
            }
        }

        _mesh.vertices = _vertices;

        int[] triangles = new int[XResolution * YResolution * 6];
        for (int ti = 0, vi = 0, y = 0; y < YResolution; y++, vi++)
        {
            for (int x = 0; x < XResolution; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + XResolution + 1;
                triangles[ti + 5] = vi + XResolution + 2;
            }
        }

        _mesh.triangles = triangles;
        _mesh.RecalculateNormals();
        //_mesh.colors32 = colours;
    }

    private Color32 _getColorForCount(int count, int maxCount)
    {
        float rangeIdx = System.Math.Max(0.0f, System.Math.Min(1.0f, (float) count / maxCount));
        int index = (int) System.Math.Floor(rangeIdx * ((float) _heatmap.Length - 1));
        return _heatmap[index];
    }

    private void OnDrawGizmos()
    {
        if (_vertices.Length == 0)
            return;

        DensityGrid grid = transform.parent.GetComponent<DensityGrid>();
        int gridMaximum = System.Math.Max(grid.MaximumDensity, 1);
        var bullets = FindObjectsOfType<Bullet>().ToList();

        // default color
        Gizmos.color = Color.black;
        for (int y = 0; y < YResolution; y++)
        {
            for (int x = 0; x < XResolution; x++)
            {
                Vector2 idx = new Vector2(x, y);
                List<int> bulletIds = grid.GetIdsForIndex(idx);

                int cellCount = 0;

                // filter ids by interactionCount
                foreach (int id in bulletIds)
                {
                    Bullet bullet = bullets.Where(bul => bul.MyIdx == id).First();

                    if (bullet.GravityCounter >= MinInteractions && bullet.GravityCounter < MaxInteractions)
                    {
                        cellCount++;
                    }
                }

                Color32 color = _getColorForCount(cellCount, gridMaximum);

                Gizmos.color = color;

                int i = y * (XResolution + 1) + x;
                Vector4 position = _vertices[i] + new Vector3(0.5f / XResolution, 0.0f, 0.5f / YResolution);
                position.w = 1;
                Vector3 center = transform.localToWorldMatrix * position;
                Vector3 size = new Vector3(transform.localToWorldMatrix.m00 * (1.0f / XResolution), 1.0f,
                    transform.localToWorldMatrix.m22 * (1.0f / YResolution));
                Gizmos.DrawCube(center, size);
            }
        }
    }
}