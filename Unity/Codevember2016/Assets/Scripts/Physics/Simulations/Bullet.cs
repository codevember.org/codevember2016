﻿using UnityEngine;
using System.Collections;
using System;

namespace Codevember16.LevelSimulation
{
    public class Bullet : SpaceObject
    {
        public static System.Random Rnd = new System.Random();
        public Vector3 Direction; // direction for each axis
        public static int NumBullets;
        public int MyIdx;
        public int GravityCounter = 0;
        public Vector2 LastGridIndex;

        void Awake()
        {
            Gravity.StaticOnTriggerEnterEvent.AddListener((grav, spaceObj) =>
            {
                if (spaceObj == this)
                {
                    GravityCounter++;
                }
            });
        }

        // Use this for initialization
        void Start()
        {
            this.name = String.Format("Bullet_{0}", MyIdx);
            float velocity = this.NextFloat();
            // the angle between each bullet
            float dirSteps = 160.0f / Math.Max(NumBullets - 1, 1);

            Direction.x = (MyIdx * dirSteps) - 80.0f;
            Direction.y = 0.0f;
            Direction.z = velocity * 500;
        }

        // check if bullets are out of the scene and delete them
        void Update()
        {
            if (
                !(this.transform.position.x > FindObjectOfType<LevelGrid>().BottomLeft.x &&
                  this.transform.position.z > FindObjectOfType<LevelGrid>().BottomLeft.y &&
                  this.transform.position.x < FindObjectOfType<LevelGrid>().TopRight.x &&
                  this.transform.position.z < FindObjectOfType<LevelGrid>().TopRight.y)
            )
            {
//                Debug.Log(String.Format("{0} had position {1} from bottom left {2} to top right {3}", this.name,
//                    this.transform.position, FindObjectOfType<LevelGrid>().BottomLeft,
//                    FindObjectOfType<LevelGrid>().TopRight));
                Destroy(this.gameObject);
            }
        }

        void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.GetComponent<Gravity>() != null)
            {
                // Debug.Log(String.Format("{0} has collided into {1}", this.name, other.gameObject.name));
                Destroy(this.gameObject);
            }
        }

        float NextFloat()
        {
            // take a random direction corresponding to 160deg.
            //            double mantissa = (Rnd.NextDouble() - 0.5f)*0.444444f;
            double mantissa = Rnd.NextDouble();
            return (float) mantissa;
        }
    }
}