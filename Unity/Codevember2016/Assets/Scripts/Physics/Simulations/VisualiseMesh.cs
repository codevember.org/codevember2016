﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Level;
using Codevember16;
using Codevember16.LevelSimulation;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class VisualiseMesh : MonoBehaviourSingleton<VisualiseMesh>
{
    public int MinInteractions = 0;
    public int MaxInteractions = 100;

    private static byte _alphaVal = 0;

    private Color32[] _heatmapColours = new Color32[]
    {
        new Color32(255, 245, 240, _alphaVal),
        new Color32(254, 224, 210, _alphaVal),
        new Color32(252, 187, 161, _alphaVal),
        new Color32(252, 146, 114, _alphaVal),
        new Color32(251, 106, 74, _alphaVal),
        new Color32(239, 59, 44, _alphaVal),
        new Color32(203, 24, 29, _alphaVal),
        new Color32(165, 15, 21, _alphaVal),
        new Color32(103, 0, 13, _alphaVal),
    };

    protected override void Update()
    {
        base.Update();

        var cells = FindObjectOfType<LevelGrid>().GetComponentsInChildren<CellObject>();

//        var TotalGridInteractions = cells.Sum(co => co.EnteredObjects.Count);
        int maxInteractions = 0; // cells.Max(co => co.EnteredObjects.Count);
        for (int i = 0; i < FindObjectOfType<LevelGrid>().CellCount.x; i++)
        {
            var cellBulletsIds =
                cells[
                    (int) FindObjectOfType<LevelGrid>().CellCount.x * (int) FindObjectOfType<LevelGrid>().CellCount.y -
                    i -
                    1].EnteredObjects;
            cellBulletsIds = InteractionFilter(cellBulletsIds);
            if (cellBulletsIds.Count > maxInteractions)
            {
                maxInteractions = cellBulletsIds.Count;
            }
        }

//        float normFactor = TotalGridInteractions/(float)MaxInteractions;

//        Debug.Log(String.Format("norm factor {0}", normFactor));

        if (maxInteractions == 0)
        {
            return;
        }

        for (var cellIdx = 0; cellIdx < cells.Length; cellIdx++)
        {
            var cellBulletsIds = cells[cellIdx].EnteredObjects;
            cellBulletsIds = InteractionFilter(cellBulletsIds);
            float normCount = cellBulletsIds.Count / (float) maxInteractions;
            int colourIdx = System.Math.Min((int) System.Math.Round(normCount * 9), 8);
//            Debug.Log(String.Format("Setting cell {0} (count {3}) with colour {1}, max interactions {2}", cellIdx,
//                colourIdx,
//                maxInteractions, cellBulletsIds.Count));
            _setCellColor(cellIdx, colourIdx);
        }
    }

    public List<FlyingSheepComponent> InteractionFilter(List<FlyingSheepComponent> bulletsList)
    {
        List<FlyingSheepComponent> filteredList = new List<FlyingSheepComponent>();

        foreach (var bullet in bulletsList)
        {
            try
            {
                if (bullet.NumSheepInteractions >= MinInteractions && bullet.NumSheepInteractions <= MaxInteractions)
                {
                    filteredList.Add(bullet);
                }
            }
            finally
            {
            }
        }

        return filteredList;
    }

    private void _setCellColor(int cellIdx, int colourIdx)
    {
        var cells = FindObjectOfType<LevelGrid>().GetComponentsInChildren<CellObject>();
        cells[cellIdx].Color = _heatmapColours[colourIdx];
    }
}