using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Codevember16
{
    public class RocketMover : MonoBehaviour
    {
        public float Velocity = 2f;
        public float TargetZ = 3f;

        protected virtual void Awake()
        {
            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * Velocity);
        }
    }
}