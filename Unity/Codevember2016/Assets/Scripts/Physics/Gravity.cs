using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine.Events;
using UnityEngine;

namespace Codevember16
{
    public class GravityEvent : UnityEvent<Gravity, SpaceObject>
    {
    }

    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(Rigidbody))]
    [ExecuteInEditMode]
    [JsonConverter(typeof(JsonPascalCasePropertyAndFieldConverter))]
    public class Gravity : MonoBehaviour
    {
        public float Range = 10f;
        public float StrengthOfAttraction = 1f;

        public float Mass
        {
            get { return _rigidBody.mass; }
            set { _rigidBody.mass = value; }
        }

        private Rigidbody _rigidBody;
        private SpaceObject _spaceObject;
        private SphereCollider _sphereCollider;


        [JsonIgnore] public List<SpaceObject> AffectingObjects = new List<SpaceObject>();

        [JsonIgnore] public Dictionary<SpaceObject, Vector3> LastAppliedForceDictionary =
            new Dictionary<SpaceObject, Vector3>();

        [JsonIgnore] public static GravityEvent StaticOnTriggerEnterEvent = new GravityEvent();
        [JsonIgnore] public static GravityEvent StaticOnTriggerExitEvent = new GravityEvent();
        [JsonIgnore] public static GravityEvent StaticOnTriggerStaysEvent = new GravityEvent();


        [JsonIgnore] public SphereCollider VisibleGravitationField;

        void Awake()
        {
            ButtonsGameScene.OnPolarityChangeEvent.AddListener(() =>
                StartCoroutine(UpdateVisibleGravitationalFieldSphereAfterFrames(2)));

            _spaceObject = this.GetComponent<SpaceObject>();
            _spaceObject = _spaceObject ?? this.transform.parent.GetComponent<SpaceObject>();
            _rigidBody = GetComponent<Rigidbody>();
            _sphereCollider = GetComponent<SphereCollider>();

            this.UpdateVisibleGravitationalFieldSphere();
        }

        private bool _gravisDisplayedOnce = false;

        void Start()
        {
            this.UpdateVisibleGravitationalFieldSphere();
        }

        private IEnumerator UpdateVisibleGravitationalFieldSphereAfterFrames(int frameCount = 2)
        {
            for (int i = 0; i <= frameCount; i++)
            {
                yield return null;
            }

            UpdateVisibleGravitationalFieldSphere();
        }

        void Update()
        {
            _sphereCollider.radius = Range;
            if (Application.isPlaying && !_gravisDisplayedOnce)
            {
                _gravisDisplayedOnce = true;
                this.UpdateVisibleGravitationalFieldSphere();
            }
        }

        private void UpdateVisibleGravitationalFieldSphere()
        {
            //Debug.Log(this.name + " update SphereCol");
            if (VisibleGravitationField != null) // && VisibleGravitationField.radius != Range
            {
                //Debug.Log(this.name + " Adjust Polarity Color according to Sheep: "+ FindObjectOfType<FlyingSheepComponent>().Polarity);

                VisibleGravitationField.transform.localScale = new Vector3(Range * 2, Range * 2, Range * 2);
                var sphereCol = new Color();

                switch (this._spaceObject.Polarity)
                {
                    case Polarity.Positive:
                    case Polarity.Negative:
                    {
                        sphereCol = this._spaceObject.Polarity != FindObjectOfType<FlyingSheepComponent>().Polarity
                            ? Color.blue
                            : Color.red;
                        break;
                    }
                    case Polarity.Neutral:
                    {
                        sphereCol = Color.grey;
                        break;
                    }
                }

                sphereCol.a = 0.125f;


                VisibleGravitationField.GetComponent<Renderer>().material.color = sphereCol;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            var so = other.GetComponent<SpaceObject>();
            if (so != null)
            {
                StaticOnTriggerEnterEvent.Invoke(this, so);
            }

            if (so != null && so.IsAttractable)
            {
                AffectingObjects.Add(so);
            }
        }

        void OnTriggerStay(Collider other)
        {
            var so = other.GetComponent<SpaceObject>();
            if (so != null)
            {
                StaticOnTriggerStaysEvent.Invoke(this, so);

                if (so.IsAttractable)
                {
                    var forceSign = GetForceSign(this._spaceObject.Polarity, so.Polarity);

                    var offset = transform.position - other.transform.position;

                    var force = StrengthOfAttraction * offset / offset.sqrMagnitude * _rigidBody.mass;
                    LastAppliedForceDictionary[so] = forceSign * force;
                    other.GetComponent<Rigidbody>().AddForce(forceSign * force);
                }
            }
        }

        void OnTriggerExit(Collider other)
        {
            var so = other.GetComponent<SpaceObject>();
            if (so != null)
            {
                StaticOnTriggerExitEvent.Invoke(this, so);
                AffectingObjects.Remove(so);
                LastAppliedForceDictionary.Remove(so);
            }
        }


        void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, Range);
        }

        public static float GetForceSign(Polarity polarity, Polarity otherPolarity)
        {
            var forceSign = 1f;
            switch (polarity)
            {
                case Polarity.Positive:
                {
                    switch (otherPolarity)
                    {
                        case Polarity.Positive:
                        {
                            forceSign = -1f;
                            break;
                        }
                        case Polarity.Negative:
                        {
                            forceSign = 1f;
                            break;
                        }
                        case Polarity.Neutral:
                        {
                            forceSign = 1f;
                            break;
                        }
                    }

                    break;
                }
                case Polarity.Negative:
                {
                    switch (otherPolarity)
                    {
                        case Polarity.Positive:
                        {
                            forceSign = 1f;
                            break;
                        }
                        case Polarity.Negative:
                        {
                            forceSign = -1f;
                            break;
                        }
                        case Polarity.Neutral:
                        {
                            forceSign = 1f;
                            break;
                        }
                    }

                    break;
                }
                case Polarity.Neutral:
                {
                    switch (otherPolarity)
                    {
                        case Polarity.Positive:
                        {
                            forceSign = 1f;
                            break;
                        }
                        case Polarity.Negative:
                        {
                            forceSign = 1f;
                            break;
                        }
                        case Polarity.Neutral:
                        {
                            forceSign = 1f;
                            break;
                        }
                    }

                    break;
                }
            }

            return forceSign;
        }
    }
}