﻿using UnityEngine;
using System.Collections;

namespace Codevember16
{
    public class DontDestroy : MonoBehaviour
    {
        public static bool WasWolf;

        void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}