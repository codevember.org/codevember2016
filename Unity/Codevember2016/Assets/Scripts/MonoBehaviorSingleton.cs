using UnityEngine;
using System.Collections;
using Prosodiya;

/// <summary>
///  Singleton-Pattern for MonoBehaviour
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance;

    public static T Instance
    {
        get
        {
            // If the Singleton has not been initialized yet, create an empty GameObject and add the Singleton to it.
            if (!_instance)
            {
                GameObject go = new GameObject();
                go.name = typeof(T) + "";
                _instance = go.AddComponent<T>();
            }

            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (!_instance && _instance != this)
        {
            _instance = (T) System.Convert.ChangeType(this, typeof(T));
        }
        else
        {
            Debug.Log(this.GetType() + " Singleton._instance already set, destroy this.");
#if UNITY_EDITOR
            Object.DestroyImmediate(this.gameObject);
#else
            Object.Destroy(this.gameObject);
#endif
            // _instance = (T)System.Convert.ChangeType(this, typeof(T));
        }
    }


    // Use this for initialization
    protected virtual void Start()
    {
    }

    // Update is called once per frame
    protected virtual void Update()
    {
    }
}