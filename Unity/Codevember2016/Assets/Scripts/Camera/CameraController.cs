﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Codevember16
{
    public class CameraController : MonoBehaviourSingleton<CameraController>
    {
        public Camera Camera;
        public bool FollowCam;
        private GameObject _goToTrack;
        private Vector3 InitialCamPosition;
        private Quaternion InitialCamRotation;
        public Vector2 ReferenceCameraResolution = Settings.ReferenceResolution;
        private bool ready = false;
        public static float AnimationTime = 2f;
        private Vector3 _offsetFollowCam = Vector3.up * 0.35f + Vector3.back * 0.62f;
        private float _followFov = 110f;
        private float _initalFov = 30f;

        protected override void Awake()
        {
            base.Awake();
            if (this.Camera == null)
            {
                this.Camera = GetComponent<Camera>();
            }

            FollowCam = false;
        }

        public void AdjustCameraToLevelDimensions(Vector2 dimensions)
        {
            this.Camera.fieldOfView *= (dimensions.x / ReferenceCameraResolution.x);
        }

        // Use this for initialization
        void Start()
        {
            InitialCamPosition = Camera.transform.position;
            InitialCamRotation = Camera.transform.rotation;
            //Debug.Log(InitialCamPosition + " " + InitialCamRotation);
            _goToTrack = FindObjectOfType<FlyingSheepComponent>().gameObject;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            //Debug.Log(InitialCamPosition + " " + InitialCamRotation);
            if (FollowCam)
            {
                if (ready)
                {
                    _goToTrack = FindObjectOfType<FlyingSheepComponent>().gameObject;
                    //Follow
                    //LeanTween.rotateY (this.gameObject, _goToTrack.transform.eulerAngles.y, AnimationTime).setEaseInOutSine ();
                    transform.position = _goToTrack.transform.position +
                                              (Vector3) (transform.localToWorldMatrix * _offsetFollowCam);
                }
            }
            else
            {
                if (ready)
                {
                    transform.position = InitialCamPosition;
                    transform.rotation = InitialCamRotation;
                }
            }
        }


        public void ToggleCameraAnimation()
        {
            // get actual sheep
            _goToTrack = FindObjectOfType<FlyingSheepComponent>().gameObject;

            ready = false;
            FollowCam = !FollowCam;
            StartCoroutine(FollowCam ? AnimateCameraDown() : AnimateCameraUp());
        }

        IEnumerator AnimateCameraDown()
        {
            //Animation
            LeanTween.move(this.gameObject, _goToTrack.transform.position + _offsetFollowCam, AnimationTime)
                .setEaseInOutSine();
            //LeanTween.rotateY (this.gameObject, _goToTrack.transform.eulerAngles.y, AnimationTime).setEaseInOutSine ();
            //Change FOV
            LeanTween.value(this.gameObject, this.Camera.fieldOfView, _followFov, AnimationTime)
                .setOnUpdate((float val) => { this.Camera.fieldOfView = val; });
            LeanTween.rotateX(this.gameObject, 10f, AnimationTime).setEaseInOutSine();
            yield return new WaitForSeconds(AnimationTime);
            //set bool ready
            ready = true;
        }


        IEnumerator AnimateCameraUp()
        {
            //Animation
            LeanTween.move(this.gameObject, InitialCamPosition, AnimationTime).setEaseInOutSine();
            //Change FOV
            LeanTween.value(this.gameObject, this.Camera.fieldOfView, _initalFov, AnimationTime)
                .setOnUpdate((float val) => { this.Camera.fieldOfView = val; });
            LeanTween.rotateX(this.gameObject, 90f, AnimationTime).setEaseInOutSine();

            yield return new WaitForSeconds(AnimationTime);
            //set bool ready
            ready = true;
        }

        public void Reset()
        {
            if (FollowCam)
            {
                ToggleCameraAnimation();
            }
        }
    }
}