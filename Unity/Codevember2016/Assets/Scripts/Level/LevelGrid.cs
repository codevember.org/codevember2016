﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Level;
using UnityEngine;
using UnityEngine.UI;

namespace Codevember16
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(GridLayoutGroup))]
    [RequireComponent(typeof(GameObjectPositioner))]
    public class LevelGrid : MonoBehaviour
    {
        public GameObject CellObject;
        private GridLayoutGroup _gridLayoutGroup;
        public Vector2 BottomLeft;
        public Vector2 TopRight;
        private GameObjectPositioner _positioner;
        public Vector2 CellCount = new Vector2(100, 100);
        public bool CalculateCellSizeFromCellCount = true;

        public GridLayoutGroup GridLayoutGroup
        {
            get
            {
                if (_gridLayoutGroup == null)
                {
                    _gridLayoutGroup = GetComponent<GridLayoutGroup>();
                }

                return _gridLayoutGroup;
            }
        }

        public GameObjectPositioner Positioner
        {
            get
            {
                if (_positioner == null)
                {
                    _positioner = GetComponent<GameObjectPositioner>();
                }

                return _positioner;
            }
        }

        void Update()
        {
            if (CalculateCellSizeFromCellCount)
            {
                var rect = GridLayoutGroup.GetComponent<RectTransform>();
                if (CellCount != null && CellCount.x > 0 && CellCount.y > 0)
                {
                    var newCellSize = new Vector2(rect.sizeDelta.x / CellCount.x, rect.sizeDelta.y / CellCount.y);

                    if (newCellSize != GridLayoutGroup.cellSize)
                    {
                        GridLayoutGroup.cellSize = newCellSize;
                        var newScale = new Vector3(newCellSize.x, newCellSize.y,
                            CellObject.GetComponent<RectTransform>().localScale.z);
                        foreach (var c in this.transform.GetComponentsInChildren<CellObject>())
                        {
                            c.GetComponent<RectTransform>().localScale = newScale;
                        }
                    }
                }
            }
        }

        public void FillGrid()
        {
            var cellCount = (GridLayoutGroup.GetComponent<RectTransform>().sizeDelta.x / GridLayoutGroup.cellSize.x) *
                            (GridLayoutGroup.GetComponent<RectTransform>().sizeDelta.y / GridLayoutGroup.cellSize.y);
//            Debug.Log("cellCount:" + cellCount);
            CellObject.transform.localScale = new Vector3(GridLayoutGroup.cellSize.x, GridLayoutGroup.cellSize.y,
                CellObject.transform.localScale.z);
            var newScale = new Vector3(GridLayoutGroup.cellSize.x, GridLayoutGroup.cellSize.y,
                CellObject.transform.localScale.z);
            CellObject.GetComponent<CellObject>().Color = new Color(0.5f, 0.5f, 0.5f, 0.05f);
//            Debug.Log("newScale: " + newScale);
            while (GridLayoutGroup.gameObject.transform.childCount < cellCount)
            {
                var go = Instantiate(CellObject, this.transform) as GameObject;
                go.transform.localScale = newScale;
            }

//            Debug.Log(String.Format("bottom left corner {0}, upper right corner {1}", BottomLeft, TopRight));
        }
    }
}