﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

namespace Codevember16
{
    [JsonConverter(typeof(JsonPascalCasePropertyAndFieldConverter))]
    public class GoalComponent : SpaceObject
    {
        public int Reward;

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<FlyingSheepComponent>() != null)
            {
            }
        }
    }
}