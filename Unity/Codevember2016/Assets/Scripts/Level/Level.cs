﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

namespace Codevember16
{
    public enum Difficulty
    {
        VeryEasy,
        Easy,
        Normal,
        Hard,
        VeryHard
    }

    public class Level
    {
        public float[,] HeatMap;

        private LevelController _levelController;

        [JsonIgnore]
        public LevelController LevelController
        {
            get
            {
                if (_levelController == null)
                {
                    _levelController = CoroutineUtils.FindObjectOfType<LevelController>();
                }

                return _levelController;
            }
        }

        private ObstacleFactory _obstacleFactory;

        [JsonIgnore]
        public ObstacleFactory ObstacleFactory
        {
            get
            {
                if (_obstacleFactory == null)
                {
                    _obstacleFactory = CoroutineUtils.FindObjectOfType<ObstacleFactory>();
                }

                return _obstacleFactory;
            }
        }

        public Difficulty Difficulty;

        public Vector2 Dimensions = Settings.ReferenceResolution;

        private List<JsonSheepzsObject> _obstacles;

        public List<JsonSheepzsObject> Obstacles
        {
            get
            {
                if (_obstacles == null)
                {
                    _obstacles = JsonConvert.DeserializeObject<List<JsonSheepzsObject>>(
                        JsonConvert.SerializeObject(LevelController.Obstacles));
                }

                return _obstacles;
            }
            set
            {
                var lis = new List<SpaceObject>();
                foreach (var o in value)
                {
                    var so = ObstacleFactory.CreateSheepzObjectFromJson(o,
                        LevelController.ObstaclesContainer.transform);
                    lis.Add(so);
                }

                _obstacles = value;
            }
        }

        private JsonSheepzsObject _sheep;

        public JsonSheepzsObject Sheep
        {
            get
            {
                if (_sheep == null)
                {
                    _sheep = JsonConvert.DeserializeObject<JsonSheepzsObject>(
                        JsonConvert.SerializeObject(LevelController.Sheep));
                }

                return _sheep;
            }
            set
            {
                _sheep = value;
                LevelController.Sheep.CopyValues(value);
            }
        }

        private Vector2 _goalSheepzWorldPosition;

        public Vector2 GoalSheepzWorldPosition
        {
            get
            {
                if (_goalSheepzWorldPosition == default(Vector2) || _goalSheepzWorldPosition == new Vector2(0, 0))
                {
                    _goalSheepzWorldPosition = LevelController.Goal.GetComponent<GameObjectPositioner>()
                        .SheepzWorldSpacePosition;
                }

                return _goalSheepzWorldPosition;
            }
            set
            {
                _goalSheepzWorldPosition = value;
                LevelController.Goal.GetComponent<GameObjectPositioner>().SheepzWorldSpacePosition = value;
            }
        }

        public Quaternion GoalRotation
        {
            get { return LevelController.Goal.transform.rotation; }
            set { LevelController.Goal.transform.rotation = value; }
        }

        public Level()
        {
        }
    }
}