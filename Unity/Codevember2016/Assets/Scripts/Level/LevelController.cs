using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

namespace Codevember16
{
    public class LevelController : MonoBehaviourSingleton<LevelController>
    {
        private Level _level;
        public GameObject SheepModel;

        public GameObject
            WolfModel;

        public GameObject PlayerSheep;

        void Awake()
        {
            ButtonsGameScene.OnPolarityChangeEvent.AddListener(ChangePolarity);
            FindObjectOfType<GameLogic>().ResetCurrentLevelNum();
            //FindObjectOfType<GameLogic>().StartNextLevel();
        }

        void ChangePolarity()
        {
            if (PlayerSheep == null) PlayerSheep = FindObjectOfType<FlyingSheepComponent>().gameObject;
            if (SheepModel == null) SheepModel = PlayerSheep.transform.GetChild(1).gameObject;
            if (WolfModel == null) WolfModel = PlayerSheep.transform.GetChild(0).gameObject;
            if (SheepModel.activeSelf)
            {
                WolfModel.SetActive(true);
                SheepModel.SetActive(false);
                PlayerSheep.GetComponent<FlyingSheepComponent>().Polarity = Polarity.Negative;
                DontDestroy.WasWolf = true;
            }
            else
            {
                WolfModel.SetActive(false);
                SheepModel.SetActive(true);
                PlayerSheep.GetComponent<FlyingSheepComponent>().Polarity = Polarity.Positive;
                DontDestroy.WasWolf = false;
            }

            //todo active and deactive models
        }

        public Level Level
        {
            get { return _level ?? (_level = new Level()); }
            set { _level = value; }
        }

        [JsonIgnore] public GameObject ObstaclesContainer;
        [JsonIgnore] public GameObject IntermediateGoalsContainer;

        [JsonIgnore] public GameObject Goal;

        private List<SpaceObject> _obstacles;

        public TextAsset LevelFile;

        public List<SpaceObject> Obstacles
        {
            get { return ObstaclesContainer.GetComponentsInChildren<SpaceObject>().ToList(); }
        }


        public FlyingSheepComponent Sheep
        {
            get { return FindObjectOfType<FlyingSheepComponent>(); }
        }

        public List<SpaceObject> IntermediateGoals = new List<SpaceObject>();

        public Vector2 Dimensions = Settings.ReferenceResolution;

        public Difficulty Difficulty = Difficulty.VeryEasy;

        protected override void Start()
        {
            base.Start();

            CameraController.Instance.AdjustCameraToLevelDimensions(Dimensions);
        }

        public void ClearLevelFromObstacles()
        {
            // Delete all Obstacles
            foreach (var so in ObstaclesContainer.GetComponentsInChildren<SpaceObject>())
            {
                //#if UNITY_EDITOR
                //                DestroyImmediate(so.gameObject);
                //#else
                //                    Destroy(so.gameObject);
                //#endif
                DestroyImmediate(so.gameObject);
            }
        }
    }
}