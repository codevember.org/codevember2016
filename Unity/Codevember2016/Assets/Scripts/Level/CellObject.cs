﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Codevember16;
using Codevember16.LevelSimulation;
using UnityEngine;

namespace Assets.Scripts.Level
{
    public class CellObject : MonoBehaviour
    {
        public Vector2 SheepzWorldPosition;

        private BoxCollider _collider;

        public List<FlyingSheepComponent> EnteredObjects = new List<FlyingSheepComponent>();

        public BoxCollider Collider
        {
            get
            {
                if (_collider == null)
                {
                    _collider = GetComponent<BoxCollider>();
                }

                return _collider;
            }
        }

        public Color Color
        {
            get { return gameObject.GetComponent<Renderer>().material.color; }
            set { gameObject.GetComponent<Renderer>().material.color = value; }
        }

        void Update()
        {
            SheepzWorldPosition = this.transform.ToSheepzWorldSpace().XZ();
        }

        void OnTriggerEnter(Collider other)
        {
            if (this.transform.GetSiblingIndex() < FindObjectOfType<LevelGrid>().CellCount.x)
            {
                return;
            }

            if (other.GetComponent<FlyingSheepComponent>() != null)
            {
                var so = other.GetComponent<FlyingSheepComponent>();
                if (so.IsAttractable && !EnteredObjects.Contains(so))
                {
                    EnteredObjects.Add(so);
//                    Debug.Log(this.name + " I was entered by:" + other.name);
                }
            }
        }
    }
}