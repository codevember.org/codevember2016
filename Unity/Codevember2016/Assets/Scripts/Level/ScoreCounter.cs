﻿using System.Collections;

namespace Codevember16
{
    public class ScoreCounter : Singleton<ScoreCounter>
    {
        public enum EScoreType
        {
            FullLevel,
            IntermediateGoal
        };

        public int Points
        {
            get { return _points; }
        }

        private int _points;

        private int[] _scoreTypePoints = {10, 5};

        // Use this for initialization
        void Start()
        {
            ResetScore();
        }

        public void ResetScore()
        {
            _points = 0;
        }


        public void AwardScore(EScoreType type, int triesCount)
        {
            int points = 0;
            switch (type)
            {
                case EScoreType.FullLevel:
                    points = System.Math.Max(1, _scoreTypePoints[(int) type] - triesCount);
                    break;

                case EScoreType.IntermediateGoal:
                    points = _scoreTypePoints[(int) type];
                    break;
            }

            _points += points;
            //UnityEngine.Debug.LogFormat ("Awarded {0} points, new Score: {1}", points, _points);
            if (_points > UnityEngine.PlayerPrefs.GetInt("HIGHSCORE"))
            {
                UnityEngine.PlayerPrefs.SetInt("HIGHSCORE", _points);
            }
        }
    }
}