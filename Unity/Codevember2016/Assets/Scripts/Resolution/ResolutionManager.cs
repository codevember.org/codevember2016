﻿using UnityEngine;
using System.Collections;
using Prosodiya;
#if UNITY_EDITOR
using UnityEditor;

#endif

public enum ResolutionGroup
{
    Undefined = 0,
    _1x = 1,
    _2x = 2,
    _4x = 4,
    Count,
}

/*
 * Singleton keeps resolution group and related methods. 
 * In Unity Editor, resolution group is updated every frame from game window size.
 * In game mode, resolution group is set once on startup.
 */

public class ResolutionManager : Singleton<ResolutionManager>
{
    public delegate void ChangedCallback();

    public ChangedCallback OnChangedCallback;

    //0,1,2,4
    private ResolutionGroup forcedResolutionGroup = ResolutionGroup.Undefined;

    private AsyncOperation unloadOp = null;


    //1x, 2x, 4x
    private ResolutionGroup resolutionGroup = global::ResolutionGroup._1x;

    public ResolutionManager()
    {
        InitResolutionGroup();

#if UNITY_EDITOR
        if (Application.isEditor == true)
        {
            EditorApplication.update += editorUpdate;
        }
#endif
    }

    public ResolutionGroup ResolutionGroup
    {
        get
        {
            if (Application.isEditor)
            {
                InitResolutionGroup();
            }

            return this.resolutionGroup;
        }
    }


    public ResolutionGroup ForcedResolutionGroup
    {
        get { return this.ForcedResolutionGroup2; }
        set
        {
            this.ForcedResolutionGroup2 = value;
            InitResolutionGroup();
        }
    }

    public ResolutionGroup ForcedResolutionGroup1
    {
        get { return ForcedResolutionGroup2; }

        set { ForcedResolutionGroup2 = value; }
    }

    public ResolutionGroup ForcedResolutionGroup2
    {
        get { return forcedResolutionGroup; }

        set { forcedResolutionGroup = value; }
    }

    private void InitResolutionGroup()
    {
        ResolutionGroup newVal;
        if ((Screen.height < 640))
        {
            newVal = ResolutionGroup._1x;
        }
        else if (Screen.height < 1080)
        {
            newVal = ResolutionGroup._2x;
        }
        else
        {
            newVal = ResolutionGroup._4x;
        }

        if (this.ForcedResolutionGroup2 != 0)
        {
            newVal = ForcedResolutionGroup2;
        }

        bool changed = this.resolutionGroup != newVal;
        this.resolutionGroup = newVal;

        if (changed == true)
        {
            if (OnChangedCallback != null)
            {
                OnChangedCallback();
            }
        }
    }

    public void UnloadUnusedAssets()
    {
        if (this.unloadOp != null)
        {
            if (this.unloadOp.isDone == false)
            {
                return;
            }
        }

        this.unloadOp = Resources.UnloadUnusedAssets();
    }

#if UNITY_EDITOR

    //Force redraw 20 frames in Unity Editor to converge NGUI anchors etc.
    private int count = 0;
    private GameObject target;

    public void UpdateViewInEditor(GameObject target)
    {
        count = 20;
        this.target = target;
    }

    private void editorUpdate()
    {
        count--;
        if (count > 0)
        {
            if (this.target != null)
            {
                EditorUtility.SetDirty(this.target);
            }
        }
    }
#endif
}