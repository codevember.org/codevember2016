using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prosodiya
{
    public abstract class Singleton<T> where T : new()
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                }

                return _instance;
            }
            set
            {
                if (_instance != null)
                {
                    throw new Exception("Singleton of Type" + _instance.GetType() + " already defined!");
                }
                else
                {
                    _instance = value;
                }
            }
        }
    }
}