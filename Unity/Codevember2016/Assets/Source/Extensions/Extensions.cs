﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Codevember16;
using Newtonsoft.Json;
using UnityEngine;
using Prosodiya;
using SeventyOneSquared;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.SceneManagement;

public static class Extensions
{
    public static string GetFullScenePath(this Transform current)
    {
        if (current.parent == null)
            return current.name;
        return current.parent.GetFullScenePath() + "_" + current.name;
    }

    public static Vector2 Divide(this Vector2 vec, Vector2 divisor)
    {
        return new Vector2(vec.x / divisor.x, vec.y / divisor.y);
    }

    public static Bounds OverlapArea(this Bounds bounds, Bounds otherBounds)
    {
        var x_overlap = Math.Max(0,
            Math.Min(bounds.max.x, otherBounds.max.x) - Math.Max(bounds.min.x, otherBounds.min.x));
        var y_overlap = Math.Max(0,
            Math.Min(bounds.max.y, otherBounds.max.y) - Math.Max(bounds.min.y, otherBounds.min.y));
        var overlap = new Bounds(
            new Vector3((Math.Max(bounds.max.x, otherBounds.max.x) - Math.Min(bounds.min.x, otherBounds.min.x)) / 2,
                (Math.Max(bounds.max.y, otherBounds.max.y) - Math.Min(bounds.min.y, otherBounds.min.y)) / 2),
            new Vector3(x_overlap, y_overlap, 0));

        return overlap;
    }

    /// <summary>
    /// Combines the path.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <param name="args">The arguments.</param>
    /// <returns></returns>
    public static string CombinePaths(this string path, params string[] args)
    {
        return args.Aggregate(path, Path.Combine);
    }

    /// <summary>
    /// Combines all paths.
    /// </summary>
    /// <param name="args">The arguments.</param>
    /// <returns></returns>
    public static string CombineAllPaths(params string[] args)
    {
        return args.Aggregate("", Path.Combine);
    }

    /// <summary>
    /// Loads all resources from path.
    /// </summary>
    /// <typeparam name="TResource">The type of the resource.</typeparam>
    /// <param name="path">The path.</param>
    /// <returns></returns>
    public static TResource[] LoadAllResourcesFromPath<TResource>(this string path) where TResource : UnityEngine.Object
    {
        var res = Resources.LoadAll<TResource>(path);
        return res;
    }

    public static TEnum ParseToEnum<TEnum>(this string str) where TEnum : struct

    {
        var ret = (TEnum) Enum.Parse(typeof(TEnum), str);
        return ret;
    }

    public static TEnum[] GetAllEnumValues<TEnum>(this TEnum en) where TEnum : struct

    {
        return (TEnum[]) Enum.GetValues(typeof(TEnum));
    }

    #region Transform and Vector Extensions

    /// <summary>
    /// Calculates the prosodiya world space position for the given unity world space position.
    /// </summary>
    /// <param name="unityWorldSpacePosition">The unity world space position.</param>
    /// <param name="referenceResolution">The reference resolution.</param>
    /// <param name="referencePPU">The reference PPU.</param>
    /// <returns></returns>
    public static Vector3 ToSheepzWorldSpace(this Vector3 unityWorldSpacePosition,
        Vector2 referenceResolution = default(Vector2), int referencePPU = 100)
    {
        if (referenceResolution == default(Vector2))
        {
            referenceResolution = Settings.ReferenceResolution;
        }

        var posXZ = new Vector2(unityWorldSpacePosition.x * referencePPU, unityWorldSpacePosition.z * referencePPU) +
                    (referenceResolution / 2);
        var prosodiyaWorldSpacePosition = new Vector3(posXZ.x, unityWorldSpacePosition.y, posXZ.y);
        return prosodiyaWorldSpacePosition;
    }

    /// <summary>
    /// Calculates the prosodiya world space position for the given unity transform.
    /// </summary>
    /// <param name="transform">The transform.</param>
    /// <param name="referenceResolution">The reference resolution.</param>
    /// <param name="referencePPU">The reference ppu.</param>
    /// <returns></returns>
    public static Vector3 ToSheepzWorldSpace(this Transform transform, Vector2 referenceResolution = default(Vector2),
        int referencePPU = 100)
    {
        var prosodiyaWorldSpacePosition = transform.position.ToSheepzWorldSpace(referenceResolution, referencePPU);
        return prosodiyaWorldSpacePosition;
    }

    public static Vector3 ToUnitySpace(this Vector3 prosodiyaWorldSpacePosition,
        Vector2 referenceResolution = default(Vector2), int referencePPU = 100)
    {
        if (referenceResolution == default(Vector2))
        {
            referenceResolution = Settings.ReferenceResolution;
        }

        var posXY = new Vector2(prosodiyaWorldSpacePosition.x / referencePPU,
                        prosodiyaWorldSpacePosition.y / referencePPU) -
                    (referenceResolution / (2 * referencePPU));
        var unityWorldSpacePosition = new Vector3(posXY.x, prosodiyaWorldSpacePosition.y, posXY.y);
        return unityWorldSpacePosition;
    }

    public static Vector2 Vec2ToUnitySpace(this Vector2 prosodiyaWorldSpacePosition,
        Vector2 referenceResolution = default(Vector2), int referencePPU = 100)
    {
        return new Vector3(prosodiyaWorldSpacePosition.x, prosodiyaWorldSpacePosition.y, 0).ToUnitySpace().XZ();
    }

    public static Vector2 OffsetToUnitySpace(this Vector2 offset, Vector2 referenceResolution = default(Vector2),
        int referencePPU = 100)
    {
        return new Vector2(offset.x / referencePPU, offset.y / referencePPU);
    }

    public static Vector2 Vec2ToSheepzWorldSpace(this Vector2 unityWorldSpacePosition,
        Vector2 referenceResolution = default(Vector2), int referencePPU = 100)
    {
        return new Vector3(unityWorldSpacePosition.x, 0, unityWorldSpacePosition.y).ToSheepzWorldSpace().XZ();
    }

    public static Vector2 XY(this Vector3 vec)
    {
        return new Vector2(vec.x, vec.y);
    }

    public static Vector2 XZ(this Vector3 vec)
    {
        return new Vector2(vec.x, vec.z);
    }

    public static bool V3Equal(this Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }

    public static bool V2Equal(this Vector2 a, Vector2 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }

    #endregion

    public static void FadeAlpha(this List<SpriteRenderer> renderes, float startAlpha, float endAlpha, float floatTime)
    {
        foreach (SpriteRenderer sr in renderes)
        {
            FadeAlpha(sr, startAlpha, endAlpha, floatTime);
        }
    }

    public static void FadeAlpha(this SpriteRenderer renderer, float startAlpha, float endAlpha, float floatTime)
    {
        LeanTween.value(renderer.gameObject, (val) =>
        {
            var origColor = renderer.color;
            renderer.color = new Color(origColor.r, origColor.g, origColor.b, val);
        }, startAlpha, endAlpha, floatTime);
    }

    public static void SetAlphaToZero(this List<SpriteRenderer> renderers)
    {
        foreach (SpriteRenderer sr in renderers)
        {
            var origColor = sr.color;
            sr.color = new Color(origColor.r, origColor.g, origColor.b, 0f);
        }
    }

    /// <summary>
    /// Gets the full path of the scene within the project..
    /// </summary>
    /// <param name="scene">The scene.</param>
    /// <param name="addExtension">if set to <c>true</c> [add extension].</param>
    /// <returns></returns>
    public static string GetFullPath(this Scene scene, bool addExtension = true)
    {
        var p = "Assets/Prosodiya_Scenes/" + scene.name;
        p = addExtension ? p + ".unity" : p;
        return p;
    }

    /// <summary>
    /// Gets the full scene path within the project.
    /// </summary>
    /// <param name="sceneName">Name of the scene.</param>
    /// <param name="addExtension">if set to <c>true</c> [add extension].</param>
    /// <returns></returns>
    public static string GetFullScenePath(this string sceneName, bool addExtension = true)
    {
        var p = "Assets/Prosodiya/_Scenes/" + sceneName;
        p = addExtension ? p + ".unity" : p;
        return p;
    }

    public static string GetFullSpritePath(this Sprite sprite)
    {
        var path = "OnlyVisibleInEditor";
#if UNITY_EDITOR
        path = AssetDatabase.GetAssetPath(sprite);
#endif
        return path;
    }

    /// <summary>
    /// Finds the nearest mono behavior.
    /// </summary>
    /// <typeparam name="TMonoBehaviour">The type of the mono behavior.</typeparam>
    /// <param name="mb">The mb.</param>
    /// <param name="hierarchyUp">if set to <c>true</c> [hierarchy up].</param>
    /// <returns></returns>
    public static TMonoBehaviour FindNearestMonoBehavior<TMonoBehaviour>(this MonoBehaviour mb, bool hierarchyUp = true)
        where TMonoBehaviour : MonoBehaviour
    {
        TMonoBehaviour mono = null;
        mono = mb.GetComponent<TMonoBehaviour>();
        if (mono)
        {
            return mono;
        }

        var transform = mb.transform;
        if (transform == null)
        {
            return null;
        }

        try
        {
            if (hierarchyUp)
            {
                var parent = transform.parent;
                while (parent != null && mono == null)
                {
                    mono = parent.gameObject.GetComponent<TMonoBehaviour>();
                    parent = parent.parent;
                }
            }
            // TODO: muss noch implementiert werden
            else
            {
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            Debug.LogError(e);
        }

        return mono;
    }

    /// <summary>
    /// Changes the case of the first char to upper case.
    /// </summary>
    /// <param name="input">The string.</param>
    /// <returns>A string with the first character in uppercase and the rest as the input</returns>
    public static string ToUpperFirst(this string input)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(input))
        {
            return string.Empty;
        }

        // Return char and concat substring.
        return char.ToUpper(input[0]) + input.Substring(1);
    }


    #region Game Object Extensions

    public static IEnumerator JumpTo(this GameObject gameObject, Vector3 targetPos, int jumps, float jumpHeight,
        float time, bool useLocalPosition = true)
    {
        var origY = gameObject.transform.localPosition.y;
        if (useLocalPosition)
        {
            LeanTween.moveLocalX(gameObject, targetPos.x, time);
        }
        else
        {
            LeanTween.moveX(gameObject, targetPos.x, time);
        }

        float jumpTime = time / (float) jumps;
        for (int i = 0; i < jumps; i++)
        {
            if (useLocalPosition)
            {
                LeanTween.moveLocalY(gameObject, origY + jumpHeight, (time / (float) jumps) / 2);
                yield return new WaitForSeconds(jumpTime / 2);
                LeanTween.moveLocalY(gameObject, origY, (time / (float) jumps) / 2);
                yield return new WaitForSeconds(jumpTime / 2);
            }
            else
            {
                LeanTween.moveY(gameObject, origY + jumpHeight, (time / (float) jumps) / 2);
                yield return new WaitForSeconds(jumpTime / 2);
                LeanTween.moveY(gameObject, origY, (time / (float) jumps) / 2);
                yield return new WaitForSeconds(jumpTime / 2);
            }
        }
    }

    public static IEnumerator JumpToProsodiyaWorldSpace(this GameObject gameObject,
        Vector3 targetPos, int jumps, float jumpHeight, float time, bool useLocalPosition = true)
    {
        return gameObject.JumpTo(targetPos.ToUnitySpace(), jumps,
            jumpHeight / Settings.ReferencePPU, time, useLocalPosition);
    }

    #endregion


    #region PDUnity Extensions

    public static void FadeIn(this PDUnity pdUnity, float time)
    {
        LeanTween.value(pdUnity.gameObject, f => pdUnity.LeanTweenAlpha = f, 0f, 1f, time);
    }

    public static void FadeOut(this PDUnity pdUnity, float time)
    {
        LeanTween.value(pdUnity.gameObject, f => pdUnity.LeanTweenAlpha = f, 1f, 0f, time);
    }

    #endregion

    /// <summary>
    /// Returns a random floating point number that is between minimum and maximum (excluded upper bound).
    /// </summary>
    /// <param name="random">The random.</param>
    /// <param name="minimum">The minimum.</param>
    /// <param name="maximum">The maximum.</param>
    /// <returns></returns>
    public static float NextFloat(this System.Random random, float minimum, float maximum)
    {
        return (float) random.NextDouble() * (maximum - minimum) + minimum;
    }

    public static void InitializeSheepzObjectFromJson(this string json, GameObject gameObject)
    {
    }

    public static List<JsonSheepzsObject> ToJsonSheepzObjectList(this List<SpaceObject> spaceObjectList)
    {
        return JsonConvert.DeserializeObject<List<JsonSheepzsObject>>(JsonConvert.SerializeObject(spaceObjectList));
    }

    public static void LoadLevelFromTextAsset(this TextAsset textAsset)
    {
        var deserialize = JsonConvert.DeserializeObject<Level>(textAsset.text);
        Debug.Log("Loaded level:" + JsonConvert.SerializeObject(deserialize, Formatting.Indented));

        // Load the sheep.
        CoroutineUtils.FindObjectOfType<LevelController>().Sheep.CopyValues(deserialize.Sheep);


        // Load the objects.
        foreach (var so in deserialize.Obstacles)
        {
            CoroutineUtils.FindObjectOfType<ObstacleFactory>()
                .CreateSheepzObjectFromJson(so,
                    CoroutineUtils.FindObjectOfType<LevelController>().ObstaclesContainer.transform);
        }
    }

    public static List<TextAsset> LoadLevelList()
    {
        var lvls = Resources.LoadAll<TextAsset>("Levels/").ToList();
        lvls.Sort((ta1, ta2) => ta1.name.CompareTo(ta2.name));
        return lvls;
    }
}