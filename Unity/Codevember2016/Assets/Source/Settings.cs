﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Prosodiya;
using UnityEngine;

namespace Codevember16
{
    public enum ScalePolicy
    {
        SHOW_ALL,
        EXACT_FIT,
        FIXED_WIDTH,
        FIXED_HEIGHT,
        NO_BORDER,
        STRETCH
    }

    public enum GameLanguage
    {
        //Deutsch
        de_DE,

        //Amerikanisches Englisch
        en_US,

        //Britisches Englisch
        en_GB,

        //Französisch
        fr_FR,

        //Spanisch
        es_ES,

        //Italienisch
        it_IT,

        //Portugisisch
        pt_PT,

        //Russisch
        ru_RU,

        //Türkisch
        tr_TR,

        //Syrisch
        ar_SY,
        UNDEFINED
    }

    public class Settings : Singleton<Settings>
    {
        public static Vector2 ReferenceResolution = new Vector2(1536, 2048);
        public static ResolutionGroup ReferenceResolutionGroup = ResolutionGroup._4x;
        public static int ReferencePPU = 100;

        /*
        [Header("Main and Game Camera ScalePolicy")]
        [Tooltip("Scale Policy for the Main and Game Camera. See ConfigCamera script.")]
        */
        //TODO: Tipo

        public ScalePolicy CamerScalePolicy
        {
            get { return CameraScalePolicy; }
            set { CameraScalePolicy = value; }
        }

        public static ScalePolicy CameraScalePolicy = ScalePolicy.FIXED_HEIGHT;

        public GameLanguage GameLanguage;
    }
}