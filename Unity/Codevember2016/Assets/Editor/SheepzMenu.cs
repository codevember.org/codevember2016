using System.IO;
using System.Linq;
using Assets.Scripts.Level;
using Codevember16;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

public class SheepzMenu : MonoBehaviour
{
    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem("SheepzMenu/Obstacles/Generate random obstacles")]
    static void GenerateRandomObstacles()
    {
        FindObjectOfType<ObstacleFactory>().GenerateRandomObstacles();
    }

    [MenuItem("SheepzMenu/Obstacles/Remove all obstacles")]
    static void RemoveAllObstacles()
    {
        var obstacles = FindObjectOfType<ObstacleFactory>().GetComponentsInChildren<SpaceObject>();
        // Debug.Log("Obstacles.Count:" + obstacles.Length);
        foreach (var so in obstacles)
        {
            DestroyImmediate(so.gameObject);
        }
    }

    [MenuItem("SheepzMenu/Obstacles/Create obstacle from json")]
    static void CreateObstacleFromJson()
    {
        var s =
            "{ \"Gravity\": {\"Mass\": 13.5272064,\"Range\": 1.293878,\"StrengthOfAttraction\": 1.96820939}, \"Name\": \"Planet(Clone)\",\"SheepzWorldPosition\": {\"x\": 891.0,\"y\": 1176.0},\"IsAttractable\": false,\"IsAttractive\": true,\"Polarity\": 0}";
        FindObjectOfType<ObstacleFactory>().CreateSheepzObjectFromJson(s);
    }

    [MenuItem("SheepzMenu/Grid and Simu/Fill Level Grid")]
    static void FillLevelGrid()
    {
        FindObjectOfType<LevelGrid>().FillGrid();
    }

    [MenuItem("SheepzMenu/Grid and Simu/Delete all LevelGrid cell objects")]
    static void RemoveCellObjects()
    {
        foreach (var c in FindObjectOfType<LevelGrid>().GetComponentsInChildren<CellObject>())
        {
            if (c.transform.GetSiblingIndex() >= 1)
            {
                DestroyImmediate(c.gameObject);
            }
        }
    }

    [MenuItem("SheepzMenu/Level/Save Level")]
    public static void SerializeLevel()
    {
        var lvl = FindObjectOfType<LevelController>().Level;
        // Debug.Log(JsonConvert.SerializeObject(lvl, Formatting.Indented));

        string path = EditorUtility.SaveFilePanel("Save Level", "Assets/Resources/Levels", "Sheepz", "json");
        if (path.Length != 0)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(lvl, Formatting.Indented));
        }
    }

    [MenuItem("SheepzMenu/Level/Load All Levels (simply Debug Output)")]
    public static void LoadAllLevels()
    {
        //   Debug.Log(("All Levels sorted:"));
        var lvls = Extensions.LoadLevelList();
        //  Debug.Log("Levels.Count:" + lvls.Count);
        lvls.ForEach(asset => Debug.Log(asset.name));
    }

    [MenuItem("SheepzMenu/Level/Load Level SheepzBiatch")]
    public static void LoadLevel()
    {
        string path = EditorUtility.OpenFilePanel("Open Level", "Assets/Resources/Levels", "json");

        var lvlJson = "";
        if (path.Length != 0)
        {
            //  Debug.Log("Path:" + path);
            lvlJson = File.OpenText(path).ReadToEnd();
        }

        if (lvlJson != "")
        {
            var deserialize = JsonConvert.DeserializeObject<Level>(lvlJson);
            // Debug.Log("Loaded level:" + JsonConvert.SerializeObject(deserialize, Formatting.Indented));

            // Load the sheep.
            FindObjectOfType<LevelController>().Sheep.CopyValues(deserialize.Sheep);
            

            foreach (var so in deserialize.Obstacles)
            {
                FindObjectOfType<ObstacleFactory>().CreateSheepzObjectFromJson(so, FindObjectOfType<LevelController>().ObstaclesContainer.transform);
            }
        }
    }
}
