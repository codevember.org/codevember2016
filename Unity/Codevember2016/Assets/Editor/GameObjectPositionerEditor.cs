﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Codevember16;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.UI
{
   

    [CustomEditor(typeof(GameObjectPositioner), true)]
    public class GameObjectPositionerEditor : Editor
    {
        private GameObjectPositioner _positioner;
        void OnEnable()
        {
            _positioner = (GameObjectPositioner)target;
            _previousSheepzsWorldPosition = _positioner.SheepzWorldSpacePosition;
        }

        private Vector2 _previousSheepzsWorldPosition;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (_positioner.SheepzWorldSpacePosition != _previousSheepzsWorldPosition)
            {
                _positioner.gameObject.transform.position = new Vector3(_positioner.SheepzWorldSpacePosition.Vec2ToUnitySpace().x, 
                    _positioner.gameObject.transform.position.y, _positioner.SheepzWorldSpacePosition.Vec2ToUnitySpace().y);
            }
            _previousSheepzsWorldPosition = _positioner.SheepzWorldSpacePosition;
        }
    }

}
