﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Music : MonoBehaviour
{
    public AudioSource sound;
    private AudioClip gameSound;
    private static bool musicExists = false;

    // Use this for initialization
    void Start()
    {
        sound = GameObject.Find("Sound").GetComponent<AudioSource>();
        gameSound = (AudioClip) Resources.Load("cv3_gamemenu");

        if (!musicExists)
        {
            DontDestroyOnLoad(sound);
            musicExists = true;
        }
        else
        {
            Destroy(this.gameObject);
            sound = GameObject.Find("Sound").GetComponent<AudioSource>();
        }

        sound.clip = gameSound;
        sound.Play();
    }

    // Update is called once per frame
    void Update()
    {
    }
}