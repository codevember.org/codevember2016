# Codevember 2016
## Something with Space? - Something with Sheeps!

Repository for the annual codevember. 

## Description
A small Unity/Android Game 

## Setup
Download Unity and load the project folder `/codevember16/` into Unity. Additionally download the Android SDK and link it into Unity. Normally things should work right out of the box.

## Misc
We suggest to peak into the manual at http://docs.unity3d.com/Manual/index.html to get a first grip at Unity's datatypes and structure (e.g. assets), but thats not absolutly necessary - we will give a intro eventually on saturday morning.

## Contributers
- Patrick Schulz
- Benedikt Beuttler
- Ido Freeman
- Lea Buchweitz
- Heiko Holz
- Tobias Rittig
- Mathias Schlenker
- and as tester (also nagger): Christoph Kreisl
